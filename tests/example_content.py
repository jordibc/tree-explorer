# Examples for tests.

good_trees = """\
;
a;
(a);
(a,b);
(,(dfd)gg);
((B:0.2,(C:0.3,D:0.4)E:0.5)A:0.1)F;
(,,(,));
(A,B,(C,D));
(A, (B, C), (D, E));
(A,B,(C,D)E)F;
(:0.1,:0.2,(:0.3,:0.4):0.5);
(:0.1,:0.2,(:0.3,:0.4):0.5):0.6;
(A:0.1,B:0.2,(C:0.3,D:0.4):0.5);
(A:0.1,B:0.2,(C:0.3,D:0.4)E:0.5)F;
((B:0.2,(C:0.3,D:0.4)E:0.5)A:0.1)F;
([&&NHX:p1=v1:p2=v2],c);
((G001575.1:0.243,G002335.1:0.2)42:0.041,G001615.1:0.246)'100.0:d__Bacteria';
(a,(b)'the_answer_is_''yes''');
(((One:0.2,Two:0.3):0.3,(Three:0.5,Four:0.3):0.2):0.3,Five:0.7):0;
((C,D)1,(A,(B,X)3)2,E)R;
([]);
((C,D)[1],(A,(B,X)[3])[2],E)[R];
( a a : 3[comment here] [comment there]);
( 'a a' : 3[comment here] [comment there]);
('Sp aeria' et.:0[&&NHX:taxid=1500313:name='Sp aeria' et.]);
('this is mean: text ''with'' (well, [1, 2...] or more) tricky characters':1);
("this is mean too: text ""with"" (well, [1, 2...] or more) tricky characters":2);
""".splitlines()

bad_trees = """\
()
(();
)(;
(()());
(()a());
((),a());
([&&NHX:a,b]);
([&&NHX:a)b];
([&&NX:p1=v1:p2=v2],c);
([&&NHX:p1=v1|p2=v2],c);
(a:b)c;
""".splitlines()


good_contents = """\
Abeillia:1[&&NHX:taxid=1507328:name=Abeillia:rank=species:sci_name=Abeillia]
1:1[&&NHX:taxid=1507327:name=Abeillia - 1507327:rank=genus:sci_name=Abeillia]
GB_GCA_001771575.1:0.243
'100.0:f__XYB2-FULL-48-7':0.078
'Sp aeria' et.:0[&&NHX:taxid=1500313:name='Sp aeria' et.
'this is mean: text ''with'' (well, [1, 2...] or more) tricky characters'
'this is mean: a text (that could have [1, 2...] or more) tricky characters'
"this is mean too: text ""with"" (well, [1, 2...] or more) tricky characters"
""".splitlines()
