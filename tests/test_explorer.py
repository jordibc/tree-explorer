"""
Test the functionality of explorer.py. To run with pytest.
"""

import sys
from os.path import abspath, dirname
sys.path.insert(0, f'{abspath(dirname(__file__))}/..')

import time
from requests import get, post, put
from urllib.parse import quote
from threading import Thread

import pytest

from ete import tree, explorer

urlbase = 'http://localhost:5000'


@pytest.fixture(scope='session', autouse=True)
def execute_before_any_test():
    # Start server in a thread if there is not one already running.
    thread = Thread(daemon=True,
                    target=lambda: explorer.run(port=5000))
    thread.start()

    time.sleep(0.2)  # give some time to the server to start before our checks


def overwrite_test_tree():
    """Make a POST request to add a simple test tree and return its result."""
    data = {'name': 'test_tree',
            'newick': '(A,B,(C,D));'}
    return post(f'{urlbase}/trees', json=data)


# The tests.

def test_not_found():
    req = get(f'{urlbase}/nonexistent')
    assert req.status_code == 404
    assert req.json()['message'].startswith('Not found:')


def test_add_tree():
    req = overwrite_test_tree()
    assert req.status_code == 201
    assert req.json()['message'] == 'ok'


def test_rename_node():
    overwrite_test_tree()

    req = put(f'{urlbase}/trees/test_tree/rename', json=[0, 'X'])
    assert req.json()['message'] == 'ok'

    assert get(f'{urlbase}/trees/test_tree/newick').json() == '(X,B,(C,D));'


def test_get_unknown_tree():
    nonexistent_tree = 'i-do-not-exist'

    for endpoint in ['', '/newick', '/draw', '/size']:
        req = get(f'{urlbase}/trees/{nonexistent_tree}{endpoint}')
        assert req.status_code == 404
        assert req.json()['message'].startswith('unknown tree')


def test_get_known_tree():
    overwrite_test_tree()

    valid_elements = ['nodebox', 'outline', 'line', 'arc', 'circle', 'text']

    trees = [x['id'] for x in get(f'{urlbase}/trees').json()]

    assert trees  # make sure we check at least one!

    for tid in trees:
        newick = get(f'{urlbase}/trees/{tid}/newick').json()
        assert newick.startswith('(') and newick.endswith(';')

        elements = get(f'{urlbase}/trees/{tid}/draw').json()
        assert all(x[0] in valid_elements for x in elements)

        assert set(get(f'{urlbase}/trees/{tid}/size').json().keys()) == \
            {'width', 'height'}


def test_get_drawers():
    drawers = get(f'{urlbase}/drawers').json()

    assert type(drawers) == list

    existing_drawers = ['RectLeafNames', 'CircLeafNames']
    assert all(x in drawers for x in existing_drawers)


def test_drawer_arguments():
    overwrite_test_tree()

    valid_requests = [
        'x=1&y=-1&w=1&h=1',
        'zx=3&zy=6',
        'drawer=Rect',
        'min_size=8',
        'panel=1',
        'rmin=5&amin=-180&amax=180',
        'x=1&y=-1&w=1&h=1&drawer=Rect&min_size=8&panel=1&zx=3&zy=6']

    invalid_requests_and_error = [
        ('x=1&y=-1&w=1&h=-1', 'invalid viewport'),
        ('zx=-3', 'zoom must be > 0'),
        ('drawer=Nonexistent', 'not a valid drawer'),
        ('min_size=-4', 'min_size must be > 0'),
        ('nonexistentarg=4', 'invalid keys')]

    for qs in valid_requests:
        assert get(f'{urlbase}/trees/test_tree/draw?{qs}').status_code == 200

    for qs, error in invalid_requests_and_error:
        req = get(f'{urlbase}/trees/test_tree/draw?{qs}')
        assert req.status_code == 400
        assert req.json()['message'].startswith(error)


def test_search():
    overwrite_test_tree()

    valid_requests = [
        'text=A',
        'text=%s' % quote('/r (A|B)'),
        'text=%s' % quote('/e is_leaf or (d is not None and d > 1)')]

    invalid_requests_and_error = [
        ('', 'missing search text'),
        ('text=/', 'invalid command'),
        ('text=%s' % quote('/e open("/etc/passwd")'), 'invalid use of'),
        ('text=%s' % quote('/e __import__("os").execv("/bin/echo", ["0wN3d"])'),
         'invalid use of')]

    for qs in valid_requests:
        assert get(f'{urlbase}/trees/test_tree/search?{qs}').status_code == 200

    for qs, error in invalid_requests_and_error:
        req = get(f'{urlbase}/trees/test_tree/search?{qs}')
        assert req.status_code == 400
        assert req.json()['message'].startswith(error)
