"""
Test the functionality of operations.py. To run with pytest.
"""

import sys
from os.path import abspath, dirname
sys.path.insert(0, f'{abspath(dirname(__file__))}/..')

from ete import Tree, operations as ops


def load_sample_tree():
    return Tree('((d,e)b,(f,g)c):0;')
    #     ╭╴b╶┬╴d
    # ╴:0╶┤   ╰╴e
    #     ╰╴c╶┬╴f
    #         ╰╴g

def strip(text):
    """Return the given text stripping the empty lines and indentation."""
    # Helps compare tree visualizations.
    indent = min(len(line) - len(line.lstrip())
                 for line in text.splitlines() if line.strip())
    return '\n'.join(line[indent:].rstrip()
        for line in text.splitlines() if line.strip())


def test_sort():
    t = load_sample_tree()

    t2 = t.copy()
    ops.sort(t2)
    assert str(t2) == str(t)

    ops.sort(t2, reverse=True)
    assert str(t2) == strip("""
            ╭╴c╶┬╴g
        ╴:0╶┤   ╰╴f
            ╰╴b╶┬╴e
                ╰╴d
    """)

    t3 = Tree('((f,g)b,c,d,((j,k)h,i)e)a;')
    ops.sort(t3, key=lambda node: len(node.children))
    assert str(t3) == strip("""
           ╭╴c
           ├╴d
        ╴a╶┼╴b╶┬╴f
           │   ╰╴g
           ╰╴e╶┬╴i
               ╰╴h╶┬╴j
                   ╰╴k
    """)


def test_set_outgroup():
    t = load_sample_tree()

    ops.set_outgroup(t[0,1])
    assert str(t) == strip("""
        ╴:0╶┬╴e
            ╰╴b╶┬╴d
                ╰╴c╶┬╴f
                    ╰╴g
    """)


    ops.set_outgroup(t['d'])
    assert str(t) == strip("""
            ╭╴d
        ╴:0╶┤   ╭╴c╶┬╴f
            ╰╴b╶┤   ╰╴g
                ╰╴e
    """)

    ops.set_outgroup(t['c'])
    assert str(t) == strip("""
            ╭╴c╶┬╴f
        ╴:0╶┤   ╰╴g
            ╰╴b╶┬╴e
                ╰╴d
    """)

    ops.set_outgroup(t['b'])
    assert str(t) == strip("""
            ╭╴b╶┬╴e
        ╴:0╶┤   ╰╴d
            ╰╴c╶┬╴f
                ╰╴g
    """)


def test_insert_intermediate_join_branch():
    t = Tree('(n1:6,n2)up;')

    n = Tree({'name': 'new'})
    ops.insert_intermediate(t['n1'], n)
    assert str(t) == strip("""
        ╴up╶┬╴new:3╶╌╴n1:3
            ╰╴n2
    """)

    t = Tree('((n1:3)i:5,n2)up;')
    # ╴up╶┬╴i:5╶╌╴n1:3
    #     ╰╴n2

    ops.join_branch(t['i'])
    assert str(t) == strip("""
        ╴up╶┬╴n1:8
            ╰╴n2
    """)


def test_move():
    t = load_sample_tree()

    ops.move(t['b'])
    assert str(t) == strip("""
            ╭╴c╶┬╴f
        ╴:0╶┤   ╰╴g
            ╰╴b╶┬╴d
                ╰╴e
    """)


def test_remove():
    t = load_sample_tree()

    ops.remove(t['c'])
    assert str(t) == strip("""
        ╴:0╶╌╴b╶┬╴d
                ╰╴e
    """)

    ops.remove(t['d'])
    assert str(t) == strip("""
        ╴:0╶╌╴b╶╌╴e
    """)


def test_to_dendrogram():
    t = Tree('((d:1,e:2)b:3,((h:4,i:5)f:6,g:7)c:8):0;')
    ops.to_dendrogram(t)
    assert all(n.dist is None for n in t.traverse())


def test_to_ultrametric():
    # The tree we are going to be using all the time:
    #    ╭╴b:3╶┬╴d:1
    #╴:0╶┤     ╰╴e:2
    #    │     ╭╴f:6╶┬╴h:4
    #    ╰╴c:8╶┤     ╰╴i:5
    #          ╰╴g:7

    # Convert the full tree, keeping distance ratios.
    t = Tree('((d:1,e:2)b:3,((h:4,i:5)f:6,g:7)c:8):0;')
    ops.to_ultrametric(t)
    assert str(t) == strip("""
            ╭╴b:11.4╶┬╴d:7.6
        ╴:0╶┤        ╰╴e:7.6
            │     ╭╴f:6╶┬╴h:5
            ╰╴c:8╶┤     ╰╴i:5
                  ╰╴g:11
    """)

    # Convert the full tree, using only topological structure.
    t = Tree('((d:1,e:2)b:3,((h:4,i:5)f:6,g:7)c:8):0;')
    ops.to_ultrametric(t, topological=True)
    assert str(t) == strip("""
            ╭╴b:9.5╶┬╴d:9.5
        ╴:0╶┤       ╰╴e:9.5
            │           ╭╴f:6.33333╶┬╴h:6.33333
            ╰╴c:6.33333╶┤           ╰╴i:6.33333
                        ╰╴g:12.6667
    """)

    # Convert a branch only.
    t = Tree('((d:1,e:2)b:3,((h:4,i:5)f:6,g:7)c:8):0;')
    ops.to_ultrametric(t['c'])
    assert str(t) == strip("""
            ╭╴b:3╶┬╴d:1
        ╴:0╶┤     ╰╴e:2
            │     ╭╴f:6╶┬╴h:5
            ╰╴c:8╶┤     ╰╴i:5
                  ╰╴g:11
    """)


def test_resolve_polytomy():
    t = Tree('((K:4,((A:1,B:2)G:3,C:1)H:0.5,D:1)I:0.5,(E:1,F:2)J:1)root;')
    #               ╭╴K:4
    #               │       ╭╴G:3╶┬╴A:1
    #       ╭╴I:0.5╶┼╴H:0.5╶┤     ╰╴B:2
    # ╴root╶┤       │       ╰╴C:1
    #       │       ╰╴D:1
    #       ╰╴J:1╶┬╴E:1
    #             ╰╴F:2

    ops.resolve_polytomy(t)

    assert str(t) == strip("""
                                        ╭╴K:4
                  ╭╴:0[&&NHX:support=0]╶┤                          ╭╴G:3╶┬╴A:1
          ╭╴I:0.5╶┤                     ╰╴H:0.5[&&NHX:support=0.0]╶┤     ╰╴B:2
    ╴root╶┤       │                                                ╰╴C:1
          │       ╰╴D:1[&&NHX:support=0.0]
          ╰╴J:1╶┬╴E:1
                ╰╴F:2
    """)


def test_farthest():
    t = Tree('((K:4,((A:1,B:2)G:3,C:1)H:0.5,D:1)I:0.5,(E:1,F:2)J:1)root;')
    #               ╭╴K:4
    #               │       ╭╴G:3╶┬╴A:1
    #       ╭╴I:0.5╶┼╴H:0.5╶┤     ╰╴B:2
    # ╴root╶┤       │       ╰╴C:1
    #       │       ╰╴D:1
    #       ╰╴J:1╶┬╴E:1
    #             ╰╴F:2

    # Farthest descendant and distance to root.
    assert ops.farthest_descendant(t) == (t['B'], 6)
    assert ops.farthest_descendant(t[1]) == (t['F'], 2)

    assert ops.farthest_descendant(t, topological=True) == (t['A'], 4)
    assert ops.farthest_descendant(t[1], topological=True) == (t['E'], 1)

    # Farther two nodes and tree diamter (distance between them).
    assert ops.farthest(t) == (t['B'], t['K'], 9.5)
    assert ops.farthest(t[1]) == (t['F'], t['E'], 3)

    assert ops.farthest(t, topological=True) == (t['A'], t['E'], 6)
    assert ops.farthest(t[1], topological=True) == (t['E'], t['F'], 2)


def test_midpoint():
    t = Tree('((K:4,((A:1,B:2)G:3,C:1)H:0.5,D:1)I:0.5,(E:1,F:2)J:1)root;')
    #               ╭╴K:4
    #               │       ╭╴G:3╶┬╴A:1
    #       ╭╴I:0.5╶┼╴H:0.5╶┤     ╰╴B:2
    # ╴root╶┤       │       ╰╴C:1
    #       │       ╰╴D:1
    #       ╰╴J:1╶┬╴E:1
    #             ╰╴F:2

    assert ops.midpoint(t) == (t['G'], 2.75)
    assert ops.midpoint(t, topological=True) == (t['H'], 1)

    ops.set_midpoint_outgroup(t)

    assert str(t) == strip("""
      ╭╴G:2.75╶┬╴A:1
    ──┤        ╰╴B:2
      │        ╭╴C:1
      ╰╴H:0.25╶┤       ╭╴K:4
               ╰╴I:0.5╶┼╴D:1
                       ╰╴J:1.5╶┬╴E:1
                               ╰╴F:2
    """)


def test_ancestors():
    t = load_sample_tree()
    t.name = 'r'  # add name to the root node

    def ancs(idn, idr=None):  # shortcut
        root = t[idr] if idr else None
        return [n.name for n in ops.ancestors(t[idn], root)]

    assert ancs('d') == ['b', 'r']
    assert ancs('f') == ['c', 'r']
    assert ancs('c') == ['r']

    assert ancs('d', 'b') == ['b']
    assert ancs('f', 'c') == ['c']
    assert ancs('c', []) == ['r']
    assert ancs('d', 'd') == []


def test_common_ancestor():
    t = load_sample_tree()
    t.name = 'r'  # add name to the root node

    def ca(ids):  # shortcut
        nodes = [t[nid] for nid in ids]
        return ops.common_ancestor(nodes).name

    assert ca('dd') == 'd'  # NOTE: this is like  ca(['d', 'd']) == 'd'
    assert ca('de') == 'b'
    assert ca('db') == 'b'
    assert ca('bd') == 'b'
    assert ca('bf') == 'r'
    assert ca('dc') == 'r'

    assert ca('d') == 'd'
    assert ca('bde') == 'b'
    assert ca('ebd') == 'b'
    assert ca('bcdefg') == 'r'

    assert ops.common_ancestor([]) is None
    assert ops.common_ancestor([t]) is t
    assert ops.common_ancestor([t['d']]) is t['d']
    assert ops.common_ancestor([Tree(), Tree()]) is None
    assert ops.common_ancestor([Tree(), Tree(), Tree()]) is None


def test_dist():
    t = Tree('((d:1,e:2)b:3,((h:4,i:5)f:6,g:7)c:8):0;')
    #    ╭╴b:3╶┬╴d:1
    #╴:0╶┤     ╰╴e:2
    #    │     ╭╴f:6╶┬╴h:4
    #    ╰╴c:8╶┤     ╰╴i:5
    #          ╰╴g:7

    dist = lambda id1, id2: ops.dist(t[id1], t[id2])  # shortcut

    assert dist('d', 'd') == 0
    assert dist('d', 'e') == 3
    assert dist('e', 'e') == 0
    assert dist('e', 'b') == 2
    assert dist('e', []) == 5
    assert dist('e', [0]) == 2
    assert dist('e', 'f') == 19
    assert dist('h', 'f') == 4

    tdist = lambda id1, id2: ops.dist(t[id1], t[id2], topological=True)

    assert tdist('d', 'd') == 0
    assert tdist('d', 'e') == 2
    assert tdist('e', 'e') == 0
    assert tdist('e', 'b') == 1
    assert tdist('e', []) == 2
    assert tdist('e', [0]) == 1
    assert tdist('e', 'f') == 4
    assert tdist('h', 'f') == 1
