"""
Tests for newick-related functions. To run with pytest.
"""

import os
PATH = os.path.abspath(f'{os.path.dirname(__file__)}/..')

import sys
sys.path.insert(0, PATH)

from tempfile import TemporaryFile

import pytest

from ete import tree  # needed before the next import (due to cython)
from ete import newick

from tests import example_content as ec


def test_loads():
    # See if we read good trees without throwing exceptions.
    for tree_text in ec.good_trees:
        t = newick.loads(tree_text)

    # Do more exhaustive tests on a single tree.
    t = newick.loads('(b:2,c:3,(e:4[&&NHX:k1=v1:k2=v2],),)a;')
    assert t.nrepr == 'a' and len(t.children) == 4
    node_b = t.children[0]
    assert node_b.nrepr == 'b:2' and not node_b.children
    node_c = t.children[1]
    assert node_c.nrepr == 'c:3' and not node_c.children
    node_d = t.children[2]
    assert node_d.nrepr == '' and len(node_d.children) == 2
    node_e = node_d.children[0]
    assert node_e.nrepr == 'e:4[&&NHX:k1=v1:k2=v2]' and not node_e.children
    node_f = node_d.children[1]
    assert node_f.nrepr == '' and not node_f.children
    node_g = t.children[3]
    assert node_g.nrepr == '' and not node_g.children


def test_read_nodes():
    # See if we read good lists of nodes without throwing exceptions.
    for tree_text in ec.good_trees:
        last_parenthesis = tree_text.rfind(')')
        if last_parenthesis != -1:
            nodes_text = tree_text[:last_parenthesis+1]
            nodes, _ = newick.read_nodes(nodes_text, newick.PARSER_DEFAULT)

    # Do more exhaustive tests on a single list of nodes.
    parser = {'leaf': [newick.NAME, newick.DIST],
              'internal': [newick.SUPPORT, newick.DIST]}
    nodes, pos = newick.read_nodes('(b:2,c:3,(e:4[&&NHX:k1=v1:k2=v2],()1),);',
                                   parser, 9)
    assert pos == 9 + len('(e:4[&&NHX:k1=v1:k2=v2],()1)')
    assert len(nodes) == 2
    assert nodes[0].nrepr == 'e:4[&&NHX:k1=v1:k2=v2]' and not nodes[0].children
    assert newick.content_repr(nodes[1], None, parser) == '1'
    assert nodes[1].support == 1
    assert newick.content_repr(nodes[0], props=['k1', 'nonexistent']) == \
        'e:4[&&NHX:k1=v1]'


def test_read_content():
    tree_text = '(a:11[&&NHX:x=foo:y=bar],b:22,,()c,(d[&&NHX:z=foo]));'
    t = newick.loads(tree_text)
    assert (t.name is None and t.dist is None and t.props == {} and
            t.nrepr == '')
    t1 = t.children[0]
    assert (t1.name == 'a' and t1.dist == 11 and
            t1.props == {'name': 'a', 'dist':11, 'x': 'foo', 'y': 'bar'} and
            t1.nrepr == 'a:11[&&NHX:x=foo:y=bar]' and t1.children == [])
    t2 = t.children[1]
    assert (t2.name == 'b' and t2.dist == 22 and
            t2.props == {'name': 'b', 'dist': 22} and
            t2.nrepr == 'b:22' and t2.children == [])
    td = t.children[-1].children[-1]
    assert (td.name == 'd' and td.dist is None and
            td.props == {'name': 'd', 'z': 'foo'} and td.nrepr == 'd[&&NHX:z=foo]')


def test_skip_quoted_name():
    assert newick.skip_quoted_name("'one two'", 0) == 9
    assert newick.skip_quoted_name("'one ''or'' two'", 0) == 16
    assert newick.skip_quoted_name('"one two"', 0) == 9
    assert newick.skip_quoted_name('"one ""or"" two"', 0) == 16
    assert newick.skip_quoted_name("pre-quote 'start end' post-quote", 10) == 21
    with pytest.raises(newick.NewickError):
        newick.skip_quoted_name('i do not start with quote', 0)
    with pytest.raises(newick.NewickError):
        newick.skip_quoted_name("'i end without a quote", 0)


def test_is_valid():
    # Good trees should be read without throwing any exception.
    for tree_text in ec.good_trees:
        newick.loads(tree_text)

    # Bad trees should all throw exceptions.
    for tree_text in ec.bad_trees:
        with pytest.raises((newick.NewickError, ValueError)):
            newick.loads(tree_text)


def test_get_props():
    for tree_text in ec.good_contents:
        props = newick.get_props(tree_text, is_leaf=True)
        assert type(props) == dict

    for tree_text in ec.good_trees:
        t = newick.loads(tree_text)
        for node in t.traverse():
            content = node.nrepr
            props = newick.get_props(content, is_leaf=False)
            assert type(props) == dict

    # See if we can even read malformed newicks with ' not correctly escaped.
    assert newick.get_props(
        "'Sp aeria' et.:0[&&NHX:taxid=1500313:name='Sp aeria' et.]", True) \
        == {'name': "'Sp aeria' et.", 'dist': 0, 'taxid': '1500313'}


def test_quote():
    assert newick.quote(' ') == "' '"
    assert newick.quote("'") == "''''"
    quoting_unneeded = ['nothing_special', '1234']
    for text in quoting_unneeded:
        assert newick.quote(text) == text
    quoting_needed = ['i am special', 'one\ntwo', 'this (or that)']
    for text in quoting_needed:
        assert newick.quote(text) != text
        assert newick.quote(text).strip("'") == text


def test_dumps():
    t = newick.loads('(a:1,(b,c)d[&&NHX:x=3]);')
    assert newick.dumps(t) == '(a:1,(b,c)d[&&NHX:x=3]);'
    assert newick.dumps(t, props=[]) == '(a:1,(b,c)d);'

    NONAME = {'name': 'name', 'read': str,   'write': lambda x: ''}
    NODIST = {'name': 'dist', 'read': float, 'write': lambda x: ''}
    parser_topo = {'leaf': [NONAME, NODIST], 'internal': [NONAME, NODIST]}
    assert newick.dumps(t, props=[], parser=parser_topo) == '(,(,));'

    for tree_text in ec.good_trees:
        if ' ' in tree_text or has_comments(tree_text):
            continue  # representation of whitespaces may change and that's okay
        t = newick.loads(tree_text)
        t_text = newick.dumps(t)
        assert t_text == tree_text
        # NOTE: we could relax this, it is asking a bit too much really

def has_comments(text):
    pos = 0
    while pos < len(text):
        if text[pos] == '[' and text[pos+1] != '&':
            return True
        pos += 1
    return False


def test_load_dump():
    for tree_text in ec.good_trees:
        with TemporaryFile(mode='w+t') as fp:
            t1 = newick.loads(tree_text)
            newick.dump(t1, fp)
            fp.seek(0)
            t2 = newick.load(fp)
            assert newick.dumps(t1) == newick.dumps(t2)


def test_from_example_files():
    # Read bigger trees in example files and see if we do not throw exceptions.
    for fname in ['aves.tree', 'GTDB_bact_r95.tree', 'HmuY.aln2.tree']:
        newick.load(open(f'{PATH}/examples/{fname}'))


def test_repr():
    t = newick.loads('(a:0.000001,b:1.3e34)c:234.34;')

    assert newick.dumps(t) == '(a:1e-06,b:1.3e+34)c:234.34;'

    def set_fmt(fmt):
        newick.DIST['write'] = lambda x: fmt % x

    set_fmt('%f')
    assert newick.dumps(t) == \
        '(a:0.000001,b:12999999999999999868938755134980096.000000)c:234.340000;'

    set_fmt('%.2f')
    assert newick.dumps(t) == \
        '(a:0.00,b:12999999999999999868938755134980096.00)c:234.34;'

    set_fmt('%E')
    assert newick.dumps(t) == '(a:1.000000E-06,b:1.300000E+34)c:2.343400E+02;'

    set_fmt('%g')  # back to normal, so pytest keeps working

    # TODO: Tests when changing more things in the representation, like:
    #     newick.REPR['internal'] = [newick.SUPPORT, newick.DIST]
