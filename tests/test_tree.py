"""
Tests for tree-related functions. To run with pytest.
"""

import os
PATH = os.path.abspath(f'{os.path.dirname(__file__)}/..')

import sys
sys.path.insert(0, PATH)

import pytest

from ete import Tree, operations as ops, text_viz as viz

from tests import example_content as ec


def test_constructor():
    node1 = Tree({'name': 'node1', 'dist': 2.7, 'k1': 'v1', 'k2': 'v2'})
    node2 = Tree(dist=22)
    node3 = Tree(name='node3', children=[node1, node2])

    assert node1.name == 'node1'
    assert node1.dist == 2.7
    assert node1.props == {'name': 'node1', 'dist': 2.7, 'k1': 'v1', 'k2': 'v2'}
    assert node1.nrepr == 'node1:2.7[&&NHX:k1=v1:k2=v2]'
    assert not node1.children
    assert (node2.name == None and node2.dist == 22 and
            node2.props == {'dist': 22})
    assert node2.nrepr == ':22'
    assert not node2.children
    assert node3.name == 'node3' and node3.dist == None
    assert node3.props == {'name': 'node3'}
    assert node3.nrepr == 'node3'
    assert node3.children == [node1, node2]

    t = Tree('(b:2,c:3,(e:4[&&NHX:k1=v1:k2=v2],),)a;')
    assert t.nrepr == 'a' and len(t.children) == 4
    node_b = t.children[0]
    assert node_b.nrepr == 'b:2' and not node_b.children
    node_c = t.children[1]
    assert node_c.nrepr == 'c:3' and not node_c.children
    node_d = t.children[2]
    assert node_d.nrepr == '' and len(node_d.children) == 2
    node_e = node_d.children[0]
    assert node_e.nrepr == 'e:4[&&NHX:k1=v1:k2=v2]' and not node_e.children
    node_f = node_d.children[1]
    assert node_f.nrepr == '' and not node_f.children
    node_g = t.children[3]
    assert node_g.nrepr == '' and not node_g.children


def test_repr():
    # A simple example.
    node1 = Tree({'name': 'node1', 'dist': 2.7, 'k1': 'v1', 'k2': 'v2'})
    node2 = Tree(dist=22)
    node3 = Tree(name='node3', children=[node1, node2])
    assert viz.to_repr(node3) == (
        "Tree({'name': 'node3'}, ["
            "Tree({'name': 'node1', 'dist': 2.7, 'k1': 'v1', 'k2': 'v2'}, []), "
            "Tree({'dist': 22}, [])])")

    # See if we recover trees from their representations (playing with eval).
    for tree_text in ec.good_trees:
        t = Tree(tree_text)
        tr = eval(viz.to_repr(t), {'Tree': Tree})  # tree recovered from its repr
        assert t.name == tr.name and t.dist == tr.dist
        assert t.props == tr.props
        assert t.nrepr == tr.nrepr
        assert len(t.children) == len(tr.children)


def test_str():
    node1 = Tree({'name': 'node1', 'dist': 2.7, 'k1': 'v1', 'k2': 'v2'})
    node2 = Tree(dist=22)
    node3 = Tree(name='node3', children=[node1, node2])
    assert str(node3) == """
╴node3╶┬╴node1:2.7[&&NHX:k1=v1:k2=v2]
       ╰╴:22
""".strip()


def test_iter():
    for tree_text in ec.good_trees:
        t = Tree(tree_text)
        visited_nodes = set()
        visited_leaves = set()
        for node in t.traverse():
            assert type(node) == Tree
            assert node not in visited_nodes and node not in visited_leaves
            visited_nodes.add(node)
            if node.is_leaf:
                visited_leaves.add(node)
        assert len(visited_nodes) == sum(1 for node in t.traverse())

        ops.update_sizes_all(t)
        assert len(visited_leaves) == t.size[1]


def test_tree_size():
    t1 = Tree('(a:4)c:2;')
    t2 = Tree('(a:4,b:5)c:2;')
    t3 = Tree('((d:8,e:7)b:6,(f:5,g:4)c:3)a:2;')

    ops.update_sizes_all(t1)
    ops.update_sizes_all(t2)
    ops.update_sizes_all(t3)

    assert t1.size == (6, 1)
    assert t2.size == (7, 2)
    assert t3.size == (16, 4)

    t1.dist = 3
    ops.update_size(t1)
    assert t1.size == (7, 1)

    t1['a'].dist += 6
    ops.update_sizes_from(t1['a'])
    assert t1['a'].size == (10, 1)
    assert t1.size == (13, 1)

    t2.children.append(t1)
    ops.update_sizes_from(t1)
    assert t2.size == (15, 3)

    t3[0].children += [t2]
    ops.update_sizes_from(t2)
    assert t3.size == (23, 7)


def test_copy():
    for tree_text in ec.good_trees:
        t1 = Tree(tree_text)
        t2 = t1.copy()
        assert (sum(1 for _ in t1.traverse()) ==
                sum(1 for _ in t2.traverse()))  # same number of nodes
        for n1, n2 in zip(t1, t2):
            assert n1 != n2  # they are not the same python object
            assert n1.nrepr == n2.nrepr  # but they do look identical


def test_getitem():
    t = Tree('((d:8,e:7)b:6,(f:5,g:4)c:3)a:2;')
    assert t['a'] == t
    assert t['f'] == t.children[1].children[0]
    assert t['z'] is None

    assert t['b'] == t[0] == t[[0]] == t[0,]
    assert t['d'] == t[0,0] == t[[0,0]] == t[0][0]
    assert t['g'] == t[1,1]
    assert t['f'] == t[-1,0] == t[-1,-2]

    with pytest.raises(IndexError):
        t[2]

    for indices in [(0, 3), (5,), (0, 0, 0)]:
        with pytest.raises(IndexError):
            t[indices]


def test_id():
    t = Tree('((d:8,e:7)b:6,(f:5,g:4)c:3)a:2;')
    assert t.id == []
    assert t['d'].id == [0, 0]
    assert t['c'].id == [1]


def test_level():
    t = Tree('((d:8,e:7)b:6,(f:5,g:4)c:3)a:2;')
    assert t.level == 0
    assert t['d'].level == 2
    assert t['c'].level == 1


def test_walk():
    t = Tree('((d,e)b,(f,g)c)a;')
    # For reference, this is what t looks like (using print(t)):
    #   ╭╴b╶┬╴d
    #╴a╶┤   ╰╴e
    #   ╰╴c╶┬╴f
    #       ╰╴g

    # Normal walk (pre-post order).
    # When nodes are visited first, they either have descendants or are leaves.
    # When nodes are visited last (coming back), their descendants are empty.
    steps = []
    for it in t.walk():
         steps.append((it.node.name, it.node_id, it.first_visit))
    assert steps == [
        ('a', (), True),
        ('b', (0,), True),  # first time visiting internal node b
        ('d', (0,0), True),  # first (and only) time visiting leaf node d
        ('e', (0,1), True),
        ('b', (0,), False),  # last time visiting b
        ('c', (1,), True),
        ('f', (1,0), True),
        ('g', (1,1), True),
        ('c', (1,), False),
        ('a', (), False)]

    # Prunning the tree while we walk.
    steps = []
    for it in t.walk():
        steps.append((it.node.name, it.node_id, it.first_visit))
        if it.node.name == 'b':
            it.descend = False  # do not follow the descendants of b
    assert steps == [
        ('a', (), True),
        ('b', (0,), True),  # it.descend has been set to False in the loop
        ('c', (1,), True),  # so we skip all the descendants of b
        ('f', (1,0), True),
        ('g', (1,1), True),
        ('c', (1,), False),
        ('a', (), False)]


def test_parent():
    t = Tree('((d:8,e:7)b:6,(f:5,g:4)c:3)a:2;')
    #   ╭╴b╶┬╴d
    #╴a╶┤   ╰╴e
    #   ╰╴c╶┬╴f
    #       ╰╴g

    assert t.up is None

    parents = [n.up.name for n in t.traverse() if n is not t]
    assert parents == ['a', 'b', 'b', 'a', 'c', 'c']

    for tree_text in ec.good_trees:
        t = Tree(tree_text)
        assert t.up is None
        for node in t.traverse():
            if node != t:
                assert node.up.children.count(node) == 1


def test_children():
    t = Tree('((d:8,e:7)b:6)a:2;')

    t2 = Tree('(f:5,g:4)c:3;')
    assert t2.up is None
    t.children.append(t2)
    assert t2.up == t

    t3 = Tree('a:1;')
    t.children.insert(0, t3)
    assert t3.up == t

    nodes1 = [Tree('x:1;'), Tree('y:1;')]
    t.children.extend(nodes1)
    assert all(n.up == t for n in nodes1)

    nodes2 = [Tree('a:0.1;'), Tree('b:0.2;')]
    t.children += nodes2
    assert all(n.up == t for n in nodes2)
    ops.update_sizes_all(t)
    assert t.size == (16, 9)

    node = t.children.pop(1)
    assert node.up is None
    ops.update_size(t)
    assert t.size == (10, 7)

    t.children.remove(t2)
    assert t2.up is None
    ops.update_sizes_all(t)
    assert t.size == (3, 5)

    t3.nrepr = 'a'
    ops.update_sizes_all(t)
    assert t.size == (3, 5)
