from setuptools import setup

from glob import glob
from os.path import isfile

from Cython.Build import cythonize


setup(
    name='tree-explorer',
    packages=['ete'],
    ext_modules=cythonize(
        glob('**/*.pyx', recursive=True),
        language_level=3,  # so it compiles for python3 (and not python2)
        compiler_directives={'embedsignature': True}),  # for call signatures
    scripts=['ete/explorer.py'],
    data_files=[
        ('share/ete/static',
         [x for x in glob('ete/static/**', recursive=True) if isfile(x)]),
        ('share/ete/examples', glob('examples/*'))],
)
