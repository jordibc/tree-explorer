# Detailed Layout

There are several parts to the project.

The module `ete` can be used independently of the rest of the code. It
has submodules to create trees (`tree.pyx`) and parse newicks
(`newick.pyx`) and trees in nexus files (`nexus.py`), create text
visual representation (`text_viz.py`), a graphical representation
(`draw.py`), do tree-related operations (`operations.py`), and explore
the trees (`explorer.py`).

The explorer contains an http server based on
[bottle](https://bottlepy.org/). It exposes in an api all the
operations that can be done to manage and represent the trees, and
also provides access to `gui.html`, which shows a gui on the browser
to explore the trees. It uses the code in `gui.js` and all the other
imported js modules in the same directory.

![gui](img/gui_small.jpg)

It also serves an entry page with a short description and an easy way
to upload new trees, `upload.html` (which uses `upload.js`).

![upload](img/upload_small.jpg)

Finally, there are tests for most of the python code in `tests`, and
examples of trees in `examples`.

The complete layout is:

```sh
readme.md
pyproject.toml  # build system (see PEP 518)
setup.py
ete/  # the module directory
    tree.pyx  # the Tree class
    newick.pyx  # newick parser
    nexus.py  # functions to handle trees in the nexus format
    text_viz.py  # text visualization of trees
    draw.py  # drawing classes and functions to represent a tree
    operations.py  # tree-related operations
    explorer.py  # http server that exposes an api to interact with trees
    __init__.py  # allow "import ete"
    static/  # files served for the gui and uploading
        gui.html  # entry point for the interactive visualization (html)
        gui.css
        upload.html  # landing page with the upload tree interface
        upload.css
        icon.png
        js/
            gui.js  # entry point for the interactive visualization (js)
            menu.js  # initialize the menus
            draw.js  # call to the api to get drawing items and convert to svg
            minimap.js  # handle the current tree view on the minimap
            zoom.js
            drag.js
            download.js
            contextmenu.js  # what happens when one right-clicks on the tree
            events.js  # hotkeys, mouse events
            search.js
            collapse.js
            label.js
            tag.js
            api.js  # handle calls to the server's api
            upload.js  # upload trees to the server
        external/  # where we keep a copy of external libraries
            readme.md  # description of where to find them
            tweakpane.min.js
            sweetalert2.min.js
tests/  # tests for the existing functionality, to run with pytest
    example_content.py
    test_tree.py
    test_newick.py
    test_draw.py
    test_explorer.py
    test_gardening.py
    test_nexus.py
examples/
    HmuY.aln2.tree  # newick with support instead of name for internal nodes
    HmuY.aln2  # alignment file corresponding to HmuY.aln2.tree (unused!)
    GTDB_bact_r95.tree  # newick file with bacterias
    aves.tree  # newick with the aves part of the ncbi tree
mkdocs.yml  # generate documentation together with the "docs" directory
docs/
    [...]
```
