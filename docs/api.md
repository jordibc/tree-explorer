# API

The server exposes a [RESTful
API](https://en.wikipedia.org/wiki/Representational_state_transfer#Applied_to_web_services),
with the following endpoints (defined in `explorer.py`):

```sh
/trees  # get info about all the existing trees
/trees/<tree_id>  # get info about the given tree
/trees/<tree_id>/draw  # get graphical elements to draw the tree
/trees/<tree_id>/newick  # get newick representation
/trees/<tree_id>/size  # get inner width and height of the full tree
/trees/<tree_id>/properties  # names of extra ones defined in any node
/trees/<tree_id>/nodecount  # total count of nodes and leaves
/trees/<tree_id>/search  # search for nodes
/trees/<tree_id>/sort  # sort branches
/trees/<tree_id>/set_outgroup  # set node as outgroup (1st child of root)
/trees/<tree_id>/move  # move branch
/trees/<tree_id>/remove  # prune branch
/trees/<tree_id>/rename  # change the name of a node
/trees/<tree_id>/reload  # load from the the original newick
/drawers  # get a list of existing drawers
/drawers/<name>  # get info about a drawer
```

This api can be directly queried with the browser (for some endpoints
that accept a GET request), or with tools such as
[curl](https://curl.se/) or [httpie](https://httpie.io/).

The frontend uses those endpoints to draw and manipulate the trees. It
works as a web application, which mainly translates the list of
graphical items coming from `/trees/<tree_id>/draw` into svgs.

It is possible to use the same backend and write a different frontend
(as a desktop application, or in a different language, or using a
different graphics library like [PixiJS](https://www.pixijs.com/))
while still taking advantage of all the optimizations done for the
drawing.
