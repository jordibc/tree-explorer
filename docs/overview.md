# Overview

There are two main parts of the Tree Explorer:

* A module (`ete`) that handles trees and their graphical
  representation.
* A server (`explorer.py`) that exposes as an api those graphical
  capabilities and also serves a gui to explore the trees
  interactively by making use of that api.

The module and the server are written in python. For efficiency
reasons, the `tree` and `newick` submodules are written in
[cython](https://cython.org/), which makes the parser about twice as
fast while making the final tree take about half the memory.

The server acts as a *backend* to the requests made by the *frontend*,
which is written in javascript.


## Project Layout

The principal files of the project are:

```sh
ete/  # the module directory
    tree.pyx  # the Tree class and basic functions
    newick.pyx  # newick parser
    draw.py  # drawing classes and functions to represent a tree
    explorer.py  # http server that exposes an api to interact with trees
    static/
        gui.html  # entry point for the interactive visualization (html)
        js/
            gui.js  # entry point for the interactive visualization (js)
```

For a more detailed view, see the [detailed layout](detailed_layout.md).
