# About Tree Explorer

Tree Explorer was an idea of Jaime Huerta-Cepas to expand the
capabilities of the [ETE Toolkit](http://etetoolkit.org/) and handle
the exploration of huge trees (trees with millions of nodes or more).

A modified version is used to explore trees in the online platform for
interactive analysis [PhyloCloud](https://phylocloud.cgmlab.org/).

It is mainly used to explore [phylogenetic
trees](https://en.wikipedia.org/wiki/Phylogenetic_tree), but it can be
used for any tree as long as it is written in the [newick
format](https://en.wikipedia.org/wiki/Newick_format).
