// Functions related to the top-right menus.

import { view, menus, to_opts, on_tree_change, on_drawer_change, show_minimap }
    from "./gui.js";
import { draw_minimap } from "./minimap.js";
import { update } from "./draw.js";
import { Pane } from "../external/tweakpane.min.js";

export { init_menus };


// Init the menus on the top with all the options we can see and change.
function init_menus(trees, drawers) {
    const pane = new Pane({
        title: "Control panel",
        container: div_menu,
        expanded: false,
    });
    menus.pane = pane;

    const tab = pane.addTab({
        pages: [
            {title: "View"},
            {title: "Representation"},
            {title: "Selections"}]});
    const [tab_view, tab_representation, tab_selections] = tab.pages;

    add_tab_view(tab_view, trees);
    add_tab_representation(tab_representation, drawers);
    add_tab_selections(tab_selections);
}


function add_tab_view(tab, trees) {
    add_folder_tree(tab, trees);
    add_folder_change(tab);
    add_folder_info(tab);
    add_folder_view(tab);
    add_folder_minimap(tab);

    tab.addBinding(view, "select_text", {label: "select text"}).on("change", () => {
        style("font").userSelect = (view.select_text ? "text" : "none");
        div_tree.style.cursor = (view.select_text ? "text" : "auto");
        div_aligned.style.cursor = (view.select_text ? "text" : "ew-resize");
        set_boxes_clickable(!view.select_text);
    });

    tab.addBinding(view, "smart_zoom", {label: "smart zoom"});
    tab.addButton({title: "share view"}).on("click", view.share_view);
    tab.addButton({title: "help"}).on("click", view.show_help);
}


function add_tab_representation(tab, drawers) {
    tab.addBinding(view.drawer, "name", {label: "drawer", options: to_opts(drawers)})
        .on("change", on_drawer_change);

    tab.addBinding(view, "min_size", {label: "collapse size", min: 1, max: 200})
        .on("change", update);

    const folder_circ = folder(tab, "circular");

    function update_with_minimap() {
        draw_minimap();
        update();
    }
    folder_circ.addBinding(view, "rmin", {label: "radius min"})
        .on("change", update_with_minimap);
    folder_circ.addBinding(view.angle, "min", {label: "angle min", min: -180, max: 180})
        .on("change", update_with_minimap);
    folder_circ.addBinding(view.angle, "max", {label: "angle max", min: -180, max: 180})
        .on("change", update_with_minimap);

    add_folder_style(tab);

    const folder_labels = folder(tab, "labels");
    menus.labels = folder_labels;

    const folder_add = folder(folder_labels, "add");

    const folder_properties = folder(folder_add, "properties");
    menus.node_properties = folder_properties;

    folder_properties.addBinding(view, "current_property",
                               {label: "properties", options: to_opts(view.node_properties)});
    folder_properties.addButton({title: "add property"}).on("click", view.label_property);

    const folder_expressions = folder(folder_add, "expressions");

    folder_expressions.addButton({title: "add expression"}).on("click", view.label_expression);
}


function add_tab_selections(tab) {
    menus.collapsed = folder(tab, "collapsed");  // filled dynamically

    menus.selections = folder(tab, "tags");  // filled dynamically with tag_node()

    add_folder_searches(tab);
}


function add_folder_tree(menu, trees) {
    const folder_tree = folder(menu, "Tree");

    folder_tree.addBinding(view, "tree", {label: "tree", options: to_opts(trees)})
      .on("change", () => {
        view.subtree = "";
        on_tree_change();
      });

    folder_tree.addBinding(view, "subtree").on("change", on_tree_change);

    const folder_download = folder(folder_tree, "Download");
    folder_download.addButton({title: "newick"}).on("click", view.download.newick);
    folder_download.addButton({title: "svg"}).on("click", view.download.svg);
    folder_download.addButton({title: "image"}).on("click", view.download.image);
}


function add_folder_change(menu) {
    const folder_change = folder(menu, "Change");

    const folder_sort = folder(folder_change, "Sort");
    folder_sort.addButton({title: "sort"}).on("click", view.sorting.sort)
    folder_sort.addBinding(view.sorting, "key");
    folder_sort.addBinding(view.sorting, "reverse");

    folder_change.addButton({title: "upload"}).on("click", view.upload);
}


function add_folder_info(menu) {
    const folder_info = folder(menu, "Info");

    const folder_nodes = folder(folder_info, "Nodes");
    folder_nodes.addBinding(view, "nnodes_visible",
        {readonly: true, label: "visible", format: x => x.toFixed(0)});
    folder_nodes.addBinding(view, "nnodes",
        {readonly: true, label: "total", format: x => x.toFixed(0)});
    folder_nodes.addBinding(view, "nleaves",
        {readonly: true, label: "leaves", format: x => x.toFixed(0)});

    const folder_position = folder(folder_info, "Pointer position");
    folder_position.addBinding(view.pos, "cx", {readonly: true, label: "x"});
    folder_position.addBinding(view.pos, "cy", {readonly: true, label: "y"});

    folder_info.addButton({title: "show details"}).on("click", view.show_tree_info);
}


function add_folder_view(menu) {
    const folder_view = folder(menu, "View");

    folder_view.addButton({title: "reset view"}).on("click", view.reset_view);

    const folder_tl = folder(folder_view, "top-left");
    folder_tl.addBinding(view.tl, "x", {readonly: true});
    folder_tl.addBinding(view.tl, "y", {readonly: true});

    const folder_zoom = folder(folder_view, "zoom");
    folder_zoom.addBinding(view.zoom, "x", {readonly: true});
    folder_zoom.addBinding(view.zoom, "y", {readonly: true});

    const folder_aligned = folder(folder_view, "align bar");
    folder_aligned.addBinding(view, "align_bar",
        {readonly: true, label: "current position"});
    folder_aligned.addBinding(view, "align_bar",
        {readonly: true, min: 0, max: 100, label: "set position"})
        .on("change", (ev) => div_aligned.style.width = `${100 - ev.value}%`);
}


function set_boxes_clickable(clickable) {
    const value = clickable ? "auto" : "none";
    Array.from(div_tree.getElementsByClassName("fg_node")).forEach(
        e => e.style.pointerEvents = value);
}


function add_folder_style(menu) {
    const folder_style = folder(menu, "style");

    const folder_node = folder(folder_style, "node");

    const folder_box = folder(folder_node, "box");

    folder_box.addBinding(view.node.box, "opacity", {min: 0, max: 0.2, step: 0.001})
      .on("change", () => style("node").opacity = view.node.box.opacity);
    folder_box.addBinding(view.node.box, "color").on("change",
        () => style("node").fill = view.node.box.color);

    const folder_dot = folder(folder_node, "dot");

    folder_dot.addBinding(view.node.dot, "radius", {min: 0, max: 10, step: 0.1})
      .on("change", () =>
        Array.from(div_tree.getElementsByClassName("nodedot")).forEach(
            e => e.setAttribute("r", view.node.dot.radius)));

    folder_dot.addBinding(view.node.dot, "opacity", {min: 0, max: 1, step: 0.01})
        .on("change", () => style("nodedot").opacity = view.node.dot.opacity);
    folder_dot.addBinding(view.node.dot, "color")
        .on("change", () => style("nodedot").fill = view.node.dot.color);

    const folder_outline = folder(folder_style, "outline");

    folder_outline.addBinding(view.outline, "opacity", {min: 0, max: 1, step: 0.1})
        .on("change", () => style("outline").fillOpacity = view.outline.opacity);
    folder_outline.addBinding(view.outline, "color")
        .on("change", () => style("outline").fill = view.outline.color);
    folder_outline.addBinding(view.outline, "width", {min: 0.1, max: 10})
        .on("change", () => style("outline").strokeWidth = view.outline.width);

    const folder_lines = folder(folder_style, "lines");

    const folder_length = folder(folder_lines, "length");

    folder_length.addBinding(view.line.length, "color")
        .on("change", () => style("distline").stroke = view.line.length.color);
    folder_length.addBinding(view.line.length, "width", {min: 0.1, max: 10})
        .on("change",
            () => style("distline").strokeWidth = view.line.length.width);

    const folder_children = folder(folder_lines, "children");

    folder_children.addBinding(view.line.children, "color")
        .on("change", () => style("childrenline").stroke = view.line.children.color);
    folder_children.addBinding(view.line.children, "width", {min: 0.1, max: 10})
        .on("change",
            () => style("childrenline").strokeWidth = view.line.children.width);
    folder_children.addBinding(view.line.children, "pattern",
          {options: to_opts(["solid", "dotted", "dotted - 2", "dotted - 4"])})
        .on("change", () => {
            const pattern = view.line.children.pattern;
            if (pattern === "solid")
                style("childrenline").strokeDasharray = "";
            else if (pattern === "dotted")
                style("childrenline").strokeDasharray = "1";
            else if (pattern === "dotted - 2")
                style("childrenline").strokeDasharray = "2";
            else if (pattern === "dotted - 4")
                style("childrenline").strokeDasharray = "4";
        });

    const folder_text = folder(folder_style, "text");

    const folder_name = folder(folder_text, "name");

    folder_name.addBinding(view.name, "color").on("change",
        () => style("name").fill = view.name.color);

    const folder_padding = folder(folder_name, "padding");

    folder_padding.addBinding(view.name.padding, "left", {min: -20, max: 200})
        .on("change", update);
    folder_padding.addBinding(view.name.padding, "vertical", {min: 0, max: 1, step: 0.01})
        .on("change", update);
    folder_name.addBinding(view.name, "font",
        {options: to_opts(["sans-serif", "serif", "monospace"])})
        .on("change", () => style("name").fontFamily = view.name.font);
    folder_name.addBinding(view.name, "max_size", {label: "max size", min: 1, max: 200})
        .on("change", update);

    const folder_panel1 = folder(folder_text, "panel 1");

    folder_panel1.addBinding(view.panel1, "color").on("change",
        () => style("panel1").fill = view.panel1.color);

    folder_panel1.addBinding(view.panel1, "font",
        {options: to_opts(["sans-serif", "serif", "monospace"])})
        .on("change", () => style("panel1").fontFamily = view.panel1.font);
    folder_panel1.addBinding(view.panel1, "max_size", {label: "max size", min: 1, max: 200})
        .on("change", update);

    folder_text.addBinding(view.font_sizes, "auto", {label: "automatic size"})
      .on("change",
        () => {
            style("font").fontSize =
                view.font_sizes.auto ? "" : `${view.font_sizes.fixed}px`;

            if (view.font_sizes.auto && view.font_sizes.scroller)
                view.font_sizes.scroller.dispose();
            else
                view.font_sizes.scroller = create_font_size_scroller();
      });

    function create_font_size_scroller() {
        return folder_text.addBinding(view.font_sizes, "fixed", {min: 0.1, max: 50})
            .on("change",
                () => style("font").fontSize = `${view.font_sizes.fixed}px`);
    }

    const folder_array = folder(folder_style, "array");

    folder_array.addBinding(view.array, "padding", {min: 0, max: 1, step: 0.01})
        .on("change", update);
}


function style(name) {
    // Based on the order in which they appear in gui.css.
    const pos = {
        "line": 1,
        "distline": 2,
        "childrenline": 3,
        "nodedot": 4,
        "font": 6,
        "name": 7,
        "node": 8,
        "outline": 9,
        "panel1": 10,
    };
    return document.styleSheets[0].cssRules[pos[name]].style;
}


function add_folder_minimap(menu) {
    const folder_minimap = folder(menu, "Minimap");

    folder_minimap.addBinding(view.minimap, "width", {min: 1, max: 100})
      .on("change", () => {
        if (view.drawer.type === "circ") {
            view.minimap.height = view.minimap.width * div_tree.offsetWidth
                                                     / div_tree.offsetHeight;
        }
        draw_minimap();
      });
    folder_minimap.addBinding(view.minimap, "height", {min: 1, max: 100})
      .on("change", () => {
        if (view.drawer.type === "circ") {
            view.minimap.width = view.minimap.height * div_tree.offsetHeight
                                                     / div_tree.offsetWidth;
        }
        draw_minimap();
      });
    folder_minimap.addBinding(view.minimap, "show")
        .on("change", () => show_minimap(view.minimap.show));
}


function add_folder_searches(menu) {
    const folder_searches = folder(menu, "searches");
    menus.searches = folder_searches;

    folder_searches.addButton({title: "new search"}).on("click", view.search);
}


// Return a new folder on element, collapsed and with the given name.
function folder(element, name) {
    return element.addFolder({title: name, expanded: false});
}
