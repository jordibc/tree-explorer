// Functions related to labels.

import { view, menus, to_opts } from "./gui.js";
import { draw_tree, get_class_name } from "./draw.js";

export { label_expression, label_property, colorize_labels };


// Put a label in the nodes and redraw the tree (with the new label too).
async function label_expression() {
    const result = await Swal.fire({
        input: "text",
        position: "top-end",
        inputPlaceholder: "Enter expression",
        showConfirmButton: false,
        preConfirm: text => {
            if (!text)
                return false;
        },
    });

    if (result.isConfirmed) {
        add_label_to_menu(result.value);
        draw_tree();
    }
}


function label_property() {
    const prop = view.current_property;

    if (prop === "name" || prop === "length")
        add_label_to_menu(prop);
    else
        add_label_to_menu(`p.get('${prop}', '')`);

    draw_tree();
}


function add_label_to_menu(expression) {
    const folder_labels = menus.labels;

    if (folder_labels.children.map(x => x.title).includes(expression)) {
        Swal.fire({
            html: "Label already exists", icon: "warning", timer: 5000,
            toast: true, position: "top-end",
        });
        return;
    }

    const folder = folder_labels.addFolder({title: expression, expanded: false});

    const colors = ["#0A0", "#A00", "#00A", "#550", "#505", "#055", "#000"];
    const nlabels = Object.keys(view.labels).length;

    view.labels[expression] = {
        nodetype: "any",
        position: "top",
        column: 0,
        color: colors[nlabels % colors.length],
        font: "sans-serif",
        max_size: 15,
    }

    view.labels[expression].remove = function() {
        delete view.labels[expression];
        folder.dispose();
        draw_tree();
    }

    const label = view.labels[expression];

    folder.addBinding(label, "nodetype",
        {options: to_opts(["leaf", "internal", "any"])}).on("change", draw_tree);

    const positions = ["top", "bottom", "left", "right", "aligned"];
    folder.addBinding(label, "position", {options: to_opts(positions)})
        .on("change", draw_tree);

    folder.addBinding(label, "column", {min: 0, max: 20, step: 1})
        .on("change", draw_tree);

    folder.addBinding(label, "color").on("change",
        () => colorize_label(expression));

    folder.addBinding(label, "font", {options: to_opts(
        ["sans-serif", "serif", "monospace"])}).on("change",
        () => colorize_label(expression));

    folder.addBinding(label, "max_size", {label: "max size", min: 1, max: 100})
        .on("change", draw_tree);

    folder.addButton({title: "remove"}).on("click", label.remove);
}


function colorize_label(expression) {
    const label = view.labels[expression];

    const clabel = get_class_name("label_" + expression);
    Array.from(div_tree.getElementsByClassName(clabel)).forEach(e => {
        e.style.fill = label.color;
        e.style.fontFamily = label.font;
    });
    Array.from(div_aligned.getElementsByClassName(clabel)).forEach(e => {
        e.style.fill = label.color;
        e.style.fontFamily = label.font;
    });
}


function colorize_labels() {
    Object.keys(view.labels).forEach(expression => colorize_label(expression));
}
