#!/usr/bin/env python3

"""
Web server to explore trees interactively.

The main endpoints are for the static files to serve the frontend
(that uses javascript), and for exposing an api to manipulate the
trees in the backend.
"""

import sys
import os
import re
import json
import platform
import gzip, bz2, zipfile, tarfile
from math import pi
from subprocess import Popen, DEVNULL
from threading import Thread
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as fmt

import brotli

from bottle import (
    get, post, put, redirect, static_file,
    request, response, error, abort, HTTPError, run)

DIR_BIN = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(DIR_BIN))  # so we can import ete w/o install

from ete import tree as ete_tree, draw, newick, nexus, operations as ops

DIR_LIB = os.path.dirname(os.path.abspath(ete_tree.__file__))


# Make sure we send the errors as json too.
@error(400)
@error(404)
def json_error(error):
    response.content_type = 'application/json'
    return json.dumps({'message': error.body})


def req_json():
    """Return what request.json would return, but gracefully aborting."""
    try:
        return json.loads(request.body.read())
    except json.JSONDecodeError as e:
        abort(400, f'bad json content: {e}')


def nice_html(content, title='Tree Explorer'):
    return f"""
<!DOCTYPE html>
<html><head><title>{title}</title>
<link rel="icon" type="image/png" href="/static/icon.png">
<link rel="stylesheet" href="/static/upload.css"></head>
<body><div class="centered">{content}</div></body></html>"""


# Routes.

@get('/')
def callback():
    if g_trees:
        if len(g_trees) == 1:
            name = list(g_trees.keys())[0]
            redirect(f'/static/gui.html?tree={name}')
        else:
            trees = '\n'.join('<li><a href="/static/gui.html?tree='
                              f'{name}">{name}</li>' for name in g_trees)
            return nice_html(f'<h1>Loaded Trees</h1><ul>\n{trees}\n</ul>')
    else:
        return nice_html("""<h1>Tree Explorer</h1>
<p>No trees loaded.</p>
<p>See the <a href="/help">help page</a> for more information.</p>""")

@get('/help')
def callback():
    return nice_html("""<h1>Help</h1>
You can go to the <a href="/static/upload.html">upload page</a>, see
a <a href="/">list of loaded trees</a>, or
<a href="http://etetoolkit.org/">consult the documentation</a>.""")

@get('/static/<path:path>')
def callback(path):
    return static_file(path, f'{DIR_LIB}/static')


@get('/trees')
def callback():
    response.content_type = 'application/json'
    return json.dumps([{'name': name, 'id': name} for name in g_trees])

# TODO: In the future we should not need this, since now the only
# property of a tree is its name, and we use it for its tree_id.
@get('/trees/<tree_id>')
def callback(tree_id):
    if tree_id in g_trees:
        return {'name': tree_id}
    else:
        abort(404, f'unknown tree {tree_id}')

@get('/trees/<tree_id>/size')
def callback(tree_id):
    width, height = load_tree(tree_id).size
    return {'width': width, 'height': height}

@get('/trees/<tree_id>/nodecount')
def callback(tree_id):
    t = load_tree(tree_id)
    return {'nnodes': sum(1 for node in t.traverse()),
            'nleaves': sum(1 for node in t.leaves())}

@get('/trees/<tree_id>/properties')
def callback(tree_id):
    t = load_tree(tree_id)
    props = set()
    for node in t.traverse():
        props |= node.props.keys()

    response.content_type = 'application/json'
    return json.dumps(list(props))

@get('/trees/<tree_id>/draw')
def callback(tree_id):
    try:
        drawer = get_drawer(tree_id, request.query)

        graphics = json.dumps(list(drawer.draw())).encode('utf8')

        response.content_type = 'application/json'
        if g_config['compress']:
            response.add_header('Content-Encoding', 'br')
            return brotli.compress(graphics)
        else:
            return graphics
    except (AssertionError, SyntaxError) as e:
        abort(400, f'when drawing: {e}')

@get('/trees/<tree_id>/search')
def callback(tree_id):
    nresults, nparents = store_search(tree_id, request.query)
    return {'message': 'ok', 'nresults': nresults, 'nparents': nparents}

@get('/trees/<tree_id>/newick')
def callback(tree_id):
    MAX_MB = 2
    response.content_type = 'application/json'
    return json.dumps(get_newick(tree_id, MAX_MB))

@put('/trees/<tree_id>/sort')
def callback(tree_id):
    node_id, key_text, reverse = req_json()
    sort(tree_id, node_id, key_text, reverse)
    return {'message': 'ok'}

@put('/trees/<tree_id>/set_outgroup')
def callback(tree_id):
    tid, subtree = get_tid(tree_id)
    if subtree:
        abort(400, 'operation not allowed with subtree')
    node_id = req_json()
    t = load_tree(tid)
    try:
        ops.set_outgroup(t[node_id])
        ops.update_sizes_all(t)
        return {'message': 'ok'}
    except AssertionError as e:
        abort(400, f'cannot root at {node_id}: {e}')

@put('/trees/<tree_id>/move')
def callback(tree_id):
    try:
        t = load_tree(tree_id)
        node_id, shift = req_json()
        ops.move(t[node_id], shift)
        return {'message': 'ok'}
    except AssertionError as e:
        abort(400, f'cannot move {node_id}: {e}')

@put('/trees/<tree_id>/remove')
def callback(tree_id):
    try:
        t = load_tree(tree_id)
        node_id = req_json()
        ops.remove(t[node_id])
        ops.update_sizes_all(t)
        return {'message': 'ok'}
    except AssertionError as e:
        abort(400, f'cannot remove {node_id}: {e}')

@put('/trees/<tree_id>/rename')
def callback(tree_id):
    try:
        t = load_tree(tree_id)
        node_id, name = req_json()
        t[node_id].name = name
        return {'message': 'ok'}
    except AssertionError as e:
        abort(400, f'cannot rename {node_id}: {e}')

@put('/trees/<tree_id>/edit')
def callback(tree_id):
    try:
        t = load_tree(tree_id)
        node_id, content = req_json()
        node = t[node_id]
        node.nrepr = content
        ops.update_sizes_all(t)
        return {'message': 'ok'}
    except (AssertionError, newick.NewickError) as e:
        abort(400, f'cannot edit {node_id}: {e}')

@put('/trees/<tree_id>/to_dendrogram')
def callback(tree_id):
    node_id = req_json()
    t = load_tree(tree_id)
    ops.to_dendrogram(t[node_id])
    ops.update_sizes_all(t)
    return {'message': 'ok'}

@put('/trees/<tree_id>/to_ultrametric')
def callback(tree_id):
    try:
        node_id = req_json()
        t = load_tree(tree_id)
        ops.to_ultrametric(t[node_id])
        ops.update_sizes_all(t)
        return {'message': 'ok'}
    except AssertionError as e:
        abort(400, f'cannot convert to ultrametric {tree_id}: {e}')

@post('/trees')
def callback():
    ids = add_trees_from_request()
    response.status = 201
    return {'message': 'ok', 'ids': ids}


# TODO: In the future we should not need these, since there will be
# only two drawers: Rect and Circ, which draw with "faces".
@get('/drawers')
def callback():
    response.content_type = 'application/json'
    return json.dumps(['RectLeafNames', 'CircLeafNames'])

@get('/drawers/<name>')
def callback(name):
    drawer_class = next(d for d in draw.get_drawers()
                        if d.__name__[len('Drawer'):] == name)
    return {'type': drawer_class.TYPE,
            'npanels': drawer_class.NPANELS}


# Logic.

# Global variables.
g_trees = {}  # 'name' -> Tree
g_config = {'compress': False}
g_searches = {}
g_thread = {}

def load_tree(tree_id):
    """Add tree to g_trees and initialize it if not there, and return it."""
    try:
        tid, subtree = get_tid(tree_id)
        return g_trees[tid][subtree]
    except (KeyError, IndexError):
        abort(404, f'unknown tree id {tree_id}')


def get_tid(tree_id):
    """Return the tree id and the subtree id, with the appropriate types."""
    # Example: 'my_tree,1,0,1,1' -> ('my_tree', [1, 0, 1, 1])
    try:
        tid, *subtree = tree_id.split(',')
        return tid, [int(n) for n in subtree]
    except ValueError:
        abort(404, f'invalid tree id {tree_id}')


def get_newick(tree_id, max_mb):
    """Return the newick representation of the given tree."""
    t = load_tree(tree_id)

    nw = newick.dumps(t)

    size_mb = len(nw) / 1e6
    if size_mb > max_mb:
        abort(400, 'newick too big (%.3g MB)' % size_mb)

    return nw


def sort(tree_id, node_id, key_text, reverse):
    """Sort the (sub)tree corresponding to tree_id and node_id."""
    t = load_tree(tree_id)

    try:
        code = compile(key_text, '<string>', 'eval')
    except SyntaxError as e:
        abort(400, f'compiling expression: {e}')

    def key(node):
        return safer_eval(code, {
            'node': node, 'name': node.name, 'is_leaf': node.is_leaf,
            'length': node.dist, 'dist': node.dist, 'd': node.dist,
            'size': node.size, 'dx': node.size[0], 'dy': node.size[1],
            'children': node.children, 'ch': node.children,
            'len': len, 'sum': sum, 'abs': abs})

    ops.sort(t[node_id], key, reverse)


# Drawers.

# TODO: Assume we have only two: rectangular and circular (and they use faces).
def get_drawer(tree_id, args):
    """Return the drawer initialized as specified in the args."""
    valid_keys = ['x', 'y', 'w', 'h', 'panel', 'zx', 'zy', 'drawer', 'min_size',
                  'collapsed_ids', 'labels', 'rmin', 'amin', 'amax']

    try:
        assert all(k in valid_keys for k in args.keys()), 'invalid keys'

        get = lambda x, default: float(args.get(x, default))  # shortcut

        viewport = ([get(k, 0) for k in ['x', 'y', 'w', 'h']]
            if all(k in args for k in ['x', 'y', 'w', 'h']) else None)
        assert viewport is None or (viewport[2] > 0 and viewport[3] > 0), \
            'invalid viewport'  # width and height must be > 0

        panel = get('panel', 0)

        zoom = (get('zx', 1), get('zy', 1))
        assert zoom[0] > 0 and zoom[1] > 0, 'zoom must be > 0'

        drawer_name = args.get('drawer', 'RectLeafNames')
        drawer_class = next(d for d in draw.get_drawers()
            if d.__name__[len('Drawer'):] == drawer_name)

        drawer_class.MIN_SIZE = get('min_size', 6)
        assert drawer_class.MIN_SIZE > 0, 'min_size must be > 0'

        limits = (None if not drawer_name.startswith('Circ') else
            (get('rmin', 0), 0,
             get('amin', -180) * pi/180, get('amax', 180) * pi/180))

        collapsed_ids = set(tuple(int(i) for i in node_id.split(',') if i != '')
            for node_id in json.loads(args.get('collapsed_ids', '[]')))

        labels = json.loads(args.get('labels', '[]'))

        searches = g_searches.get(tree_id)

        return drawer_class(load_tree(tree_id), viewport, panel, zoom,
                            limits, collapsed_ids, labels, searches)
    except StopIteration:
        abort(400, f'not a valid drawer: {drawer_name}')
    except (ValueError, AssertionError) as e:
        abort(400, str(e))


# Search.

def store_search(tree_id, args):
    """Store the results and parents of a search and return their numbers."""
    if 'text' not in args:
        abort(400, 'missing search text')

    text = args.pop('text').strip()
    func = get_search_function(text)

    try:
        results = set(node for node in load_tree(tree_id) if func(node))

        parents = set()
        for node in results:
            up = node.up
            while up and up not in parents:
                parents.add(up)
                up = up.up

        g_searches.setdefault(tree_id, {})[text] = (results, parents)

        return len(results), len(parents)
    except HTTPError:
        raise
    except Exception as e:
        abort(400, f'evaluating expression: {e}')


def get_search_function(text):
    """Return a function of a node that returns True for the searched nodes."""
    if text.startswith('/'):  # command-based search
        return get_command_search(text)
    elif text == text.lower():  # case-insensitive search
        return lambda node: text in node.props.get('name', '').lower()
    else:  # case-sensitive search
        return lambda node: text in node.props.get('name', '')


def get_command_search(text):
    """Return the appropriate node search function according to the command."""
    parts = text.split(None, 1)
    if parts[0] not in ['/r', '/e']:
        abort(400, 'invalid command %r' % parts[0])
    if len(parts) != 2:
        abort(400, 'missing argument to command %r' % parts[0])

    command, arg = parts
    if command == '/r':  # regex search
        return lambda node: re.search(arg, node.props.get('name', ''))
    elif command == '/e':  # eval expression
        return get_eval_search(arg)
    else:
        abort(400, 'invalid command %r' % command)


def get_eval_search(expression):
    """Return a function of a node that evaluates the given expression."""
    try:
        code = compile(expression, '<string>', 'eval')
    except SyntaxError as e:
        abort(400, f'compiling expression: {e}')

    return lambda node: safer_eval(code, {
        'node': node, 'parent': node.up, 'up': node.up,
        'name': node.name, 'is_leaf': node.is_leaf,
        'length': node.dist, 'dist': node.dist, 'd': node.dist,
        'properties': node.props, 'props': node.props, 'p': node.props,
        'get': dict.get,
        'children': node.children, 'ch': node.children,
        'size': node.size, 'dx': node.size[0], 'dy': node.size[1],
        'regex': re.search,
        'startswith': str.startswith, 'endswith': str.endswith,
        'upper': str.upper, 'lower': str.lower, 'split': str.split,
        'any': any, 'all': all, 'len': len,
        'sum': sum, 'abs': abs, 'float': float, 'pi': pi})


def safer_eval(code, context):
    """Return a safer version of eval(code, context)."""
    for name in code.co_names:
        if name not in context:
            abort(400, 'invalid use of %r during evaluation' % name)
    return eval(code, {'__builtins__': {}}, context)


# Add trees.

def add_trees_from_request():
    """Add trees to the global var g_trees and return a dict of {name: id}."""
    try:
        if request.content_type.startswith('application/json'):
            trees = [req_json()]  # we have only one tree
            parser = newick.PARSER_DEFAULT
        else:
            trees = get_trees_from_form()
            parser = get_parser(request.forms['internal'])

        for tree in trees:
            t = newick.loads(tree['newick'], parser)
            ops.update_sizes_all(t)
            g_trees[tree['name']] = t

        return {tree['name']: tree['name'] for tree in trees}
        # TODO: tree ids are already equal to their names, so in the future
        # we could remove the need to send back their "ids".
    except (newick.NewickError, ValueError) as e:
        abort(400, f'malformed tree - {e}')


def get_parser(internal):
    """Return parser given the internal nodes main property interpretation."""
    p = {'name': newick.NAME, 'support': newick.SUPPORT}[internal]  # (()p:d);
    return dict(newick.PARSER_DEFAULT, internal=[p, newick.DIST])


def get_trees_from_form():
    """Return list of dicts with tree info read from a form in the request."""
    if 'trees' in request.files:
        try:
            fu = request.files['trees']  # bottle FileUpload object
            return get_trees_from_file(fu.filename, fu.file)
        except (gzip.BadGzipFile, UnicodeDecodeError) as e:
            abort(400, f'when reading {fupload.filename}: {e}')
    else:
        return [{'name': request.forms['name'],
                 'newick': request.forms['newick']}]


def get_trees_from_file(filename, fileobject=None):
    """Return list of {'name': ..., 'newick': ...} extracted from file."""
    fileobject = fileobject or open(filename, 'rb')

    trees = []
    def extend(btext, fname):
        name = os.path.splitext(os.path.basename(fname))[0]  # /d/n.e -> n
        trees.extend(get_trees_from_nexus_or_newick(btext, name))

    if filename.endswith('.zip'):
        zf = zipfile.ZipFile(fileobject)
        for fname in zf.namelist():
            extend(zf.read(fname), fname)
    elif filename.endswith('.tar'):
        tf = tarfile.TarFile(fileobj=fileobject)
        for fname in tf.getnames():
            extend(tf.extractfile(fname).read(), fname)
    elif filename.endswith('.tar.gz') or filename.endswith('.tgz'):
        tf = tarfile.TarFile(fileobj=gzip.GzipFile(fileobj=fileobject))
        for fname in tf.getnames():
            extend(tf.extractfile(fname).read(), fname)
    elif filename.endswith('.gz'):
        extend(gzip.GzipFile(fileobj=fileobject).read(), filename)
    elif filename.endswith('.bz2'):
        extend(bz2.BZ2File(fileobject).read(), filename)
    else:
        extend(fileobject.read(), filename)

    return trees


def get_trees_from_nexus_or_newick(btext, name_newick):
    """Return list of {'name': ..., 'newick': ...} extracted from btext."""
    text = btext.decode('utf8').strip()

    try:  # we first try to read it as a nexus file
        trees = nexus.get_trees(text)
        return [{'name': name, 'newick': nw} for name, nw in trees.items()]
    except nexus.NexusError:  # if it isn't, we assume the text is a newick
        return [{'name': name_newick, 'newick': text}]  # only one tree!


# Explore.

def explore(tree, name=None, quiet=True, port=5000):
    """Run the web server, add tree and open a browser to visualize it."""
    ops.update_sizes_all(tree)  # to make sure we are ready to draw

    g_trees[name or make_name()] = tree

    launch_browser(port)

    if not g_thread:
        g_thread['main'] = Thread(daemon=True,
                                  target=lambda: run(quiet=quiet, port=port))
        g_thread['main'].start()


def make_name():
    """Return a unique tree name like 'tree-<number>'."""
    tnames = [name for name in g_trees
              if name.startswith('tree-') and name[len('tree-'):].isdecimal()]
    n = max((int(name[len('tree-'):]) for name in tnames), default=0) + 1
    print(f'New tree name: tree-{n}')
    return f'tree-{n}'


def launch_browser(port):
    """Try to open a browser window in a different process."""
    try:
        command = {'Linux': 'xdg-open', 'Darwin': 'open'}[platform.system()]
        Popen([command, f'http://localhost:{port}'],
              stdout=DEVNULL, stderr=DEVNULL)
    except (KeyError, FileNotFoundError) as e:
        print(f'Explorer available at http://localhost:{port}')



if __name__ == '__main__':
    parser = ArgumentParser(description=__doc__, formatter_class=fmt)
    add = parser.add_argument  # shortcut
    add('FILE', help='file with the newick tree representation')
    add('-i', '--internal', choices=['name', 'support'], default='name',
        help='how to interpret the content of internal nodes')
    add('-c', '--compress', action='store_true', help='send compressed data')
    add('-p', '--port', type=int, default=5000, help='server port number')
    add('-v', '--verbose', action='store_true', help='be verbose')
    args = parser.parse_args()

    try:
        for tree in get_trees_from_file(args.FILE):  # read trees
            t = newick.loads(tree['newick'], get_parser(args.internal))
            ops.update_sizes_all(t)
            g_trees[tree['name']] = t

        launch_browser(args.port)

        options = {'port': args.port, 'quiet': not args.verbose}  # for run()
        Thread(daemon=True, target=run, kwargs=options).start()

        input('Press enter to stop the server and finish.\n')
    except (FileNotFoundError, newick.NewickError, ValueError) as e:
        sys.exit(f'Error using tree from {args.FILE}: {e}')
    except (OSError, OverflowError) as e:
        sys.exit(f'Error listening at port {args.port}: {e}')
    except KeyboardInterrupt:
        pass  # bye!
