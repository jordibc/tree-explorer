"""
Classes and functions for drawing a tree.
"""

from math import sin, cos, pi, sqrt, atan2
from collections import namedtuple, OrderedDict
import random
import re  # so it can be used in label expressions

Size = namedtuple('Size', 'dx dy')  # size of a 2D shape (sizes are always >= 0)
Box = namedtuple('Box', 'x y dx dy')  # corner and size of a 2D shape
# They are all "generalized coordinates" (can be radius and angle, say).

# Description of a label that we want to add to the representation of a node.
Label = namedtuple('Label', 'code text_type node_type position column')


# The convention for coordinates is:
#   x increases to the right, y increases to the bottom.
#
#  +-----> x          +------.
#  |                   \     .
#  |                    \   . a    (the angle thus increases clockwise too)
#  v y                 r \.
#
# This is the convention normally used in computer graphics, including SVGs,
# HTML Canvas, Qt, and PixiJS.
#
# The boxes (shapes) we use are:
#
# * Rectangle            w
#                 x,y +-----+      so (x,y) is its (left,top) corner
#                     |     | h    and (x+w,y+h) its (right,bottom) one
#                     +-----+
#
# * Annular sector       dr
#                  r,a .----.
#                     .     .      so (r,a) is its (inner,smaller-angle) corner
#                      \   . da    and (r+dr,a+da) its (outer,bigger-angle) one
#                       \.

# Drawing.

class Drawer:
    """Base class (needs subclassing with extra functions to draw)."""

    MIN_SIZE = 6  # anything that has less pixels will be outlined
    MIN_SIZE_CONTENT = 4

    NPANELS = 1  # number of drawing panels (including the aligned ones)
    TYPE = 'base'  # can be 'rect' or 'circ' for working drawers

    def __init__(self, tree, viewport=None, panel=0, zoom=(1, 1),
                 limits=None, collapsed_ids=None, labels=None, searches=None):
        self.tree = tree
        self.viewport = Box(*viewport) if viewport else None
        self.panel = panel
        self.zoom = zoom
        self.xmin, self.xmax, self.ymin, self.ymax = limits or (0, 0, 0, 0)
        self.collapsed_ids = collapsed_ids or set()  # manually collapsed
        self.labels = [read_label(label) for label in (labels or [])]
        self.searches = searches or {}  # looks like {text: (results, parents)}

    def draw(self):
        """Yield graphic elements to draw the tree."""
        self.outline = None  # box surrounding the current collapsed nodes
        self.collapsed = []  # nodes that are curretly collapsed together
        self.nodeboxes = []  # boxes surrounding all nodes and collapsed boxes
        self.nodes_dx = [0]  # nodes dx, from root to current (with subnodes)
        self.bdy_dys = [[]]  # lists of branch dys and total dys

        point = self.xmin, self.ymin

        for it in self.tree.walk():
            graphics = []  # list that will contain the graphic elements to draw

            if it.first_visit:
                point = self.on_first_visit(point, it, graphics)
            else:
                point = self.on_last_visit(point, it, graphics)

            yield from graphics

        if self.outline:
            yield from self.get_outline()  # send the last surrounding outline

        if self.panel == 0:
            # Draw in preorder the boxes we got in postorder (to stack well).
            yield from self.nodeboxes[::-1]

    def on_first_visit(self, point, it, graphics):
        """Update list of graphics to draw and return new position."""
        box_node = make_box(point, self.node_size(it.node))
        x, y = point

        # Skip if not in viewport.
        if not self.in_viewport(box_node):
            self.bdy_dys[-1].append( (box_node.dy / 2, box_node.dy) )
            it.descend = False  # skip children
            return x, y + box_node.dy

        # Deal with outlines for collapsed nodes.
        is_manually_collapsed = it.node_id in self.collapsed_ids

        if self.outline and (is_manually_collapsed or
                             not self.is_small(self.outline)):
            graphics += self.get_outline()  # don't stack on the same outline

        if is_manually_collapsed or self.is_small(box_node):
            self.nodes_dx[-1] = max(self.nodes_dx[-1], box_node.dx)
            self.collapsed.append(it.node)
            self.outline = stack(self.outline, box_node)
            it.descend = False  # skip children
            return x, y + box_node.dy

        # If we arrive here, the node will be fully drawn (eventually).

        if self.outline:  # if there were previously collapsed nodes...
            graphics += self.get_outline()  # draw and reset them

        self.bdy_dys.append([])  # we will store new branch dys and total dys

        dx, dy = self.content_size(it.node)
        if it.node.is_leaf:
            return self.on_last_visit((x + dx, y + dy), it, graphics)
        else:
            self.nodes_dx.append(0)
            return x + dx, y

    def on_last_visit(self, point, it, graphics):
        """Update list of graphics to draw and return new position."""
        if self.outline:
            graphics += self.get_outline()

        x_after, y_after = point
        dx, dy = self.content_size(it.node)
        x_before, y_before = x_after - dx, y_after - dy

        content_graphics = list(self.draw_content(it.node, (x_before, y_before)))
        graphics += content_graphics

        ndx = (drawn_size(content_graphics, self.get_box).dx if it.node.is_leaf
               else (dx + self.nodes_dx.pop()))
        self.nodes_dx[-1] = max(self.nodes_dx[-1], ndx)

        box = Box(x_before, y_before, ndx, dy)
        result_of = [text for text,(results,_) in self.searches.items()
                        if it.node in results]
        self.nodeboxes += self.draw_nodebox(it.node, it.node_id, box, result_of)

        return x_before, y_after

    def draw_content(self, node, point):
        """Yield the node content's graphic elements."""
        x, y = point
        dx, dy = self.content_size(node)

        if not self.in_viewport(Box(x, y, dx, dy)):
            return

        # Find branch dy of first child (bdy0), last (bdy1), and self (bdy).
        bdy_dys = self.bdy_dys.pop()  # bdy_dys[i] == (bdy, dy)
        bdy0 = bdy1 = dy / 2  # branch dys of the first and last children
        if bdy_dys:
            bdy0 = bdy_dys[0][0]
            bdy1 = sum(bdy_dy[1] for bdy_dy in bdy_dys[:-1]) + bdy_dys[-1][0]
        bdy = (bdy0 + bdy1) / 2  # this node's branch dy
        self.bdy_dys[-1].append( (bdy, dy) )

        # Draw the branch line ("distline") and a line spanning all children.
        if self.panel == 0:
            if dx > 0:
                parent_of = [text for text,(_,parents) in self.searches.items()
                                if node in parents]
                yield from self.draw_distline((x, y + bdy), (x + dx, y + bdy),
                                                parent_of)

                yield from self.draw_nodedot((x + dx, y + bdy))

            if bdy0 != bdy1:
                yield from self.draw_childrenline((x + dx, y + bdy0),
                                                  (x + dx, y + bdy1))

        yield from self.draw_node(node, point, bdy)

    def get_outline(self):
        """Yield the outline representation."""
        result_of = [text for text,(results,parents) in self.searches.items()
            if any(node in results or node in parents
                   for node in self.collapsed)]

        graphics = []  # will contain the graphic elements to draw

        node0 = self.collapsed[0]
        uncollapse = len(self.collapsed) == 1 and node0.is_leaf

        if uncollapse:
            self.bdy_dys.append([])
            x, y, _, _ = self.outline
            graphics += self.draw_content(node0, (x, y))
        else:
            if self.panel == 0:
                graphics += self.draw_outline()  # we update self.bdy_dys too
            graphics += self.draw_collapsed()

        self.collapsed = []

        ndx = max(self.outline.dx, drawn_size(graphics, self.get_box).dx)
        self.nodes_dx[-1] = max(self.nodes_dx[-1], ndx)

        outline = self.flush_outline(ndx)
        name, props = ((node0.name, node0.props) if uncollapse else
                       ('(collapsed)', {}))
        box = draw_nodebox(outline, name, props, node0.id, result_of)
        self.nodeboxes.append(box)

        yield from graphics

    def draw_outline(self, *args, **kwargs):
        """Yield outline with all the skeleton points."""
        x, y, dx, dy = self.outline
        _, zy = self.zoom

        points = points_from_nodes(self.collapsed, (x, y),
                                   self.MIN_SIZE_CONTENT/zy,
                                   *args, **kwargs)  # for subclasses

        y1 = points[-1][1]  # last point's y (it is at branch position)
        self.bdy_dys[-1].append( (y1 - y, dy) )

        yield draw_outline(points)

    def flush_outline(self, minimum_dx=0):
        """Return box outlining collapsed nodes and reset current outline."""
        x, y, dx, dy = self.outline
        self.outline = None
        return Box(x, y, max(dx, minimum_dx), dy)

    def dx_fitting_texts(self, texts, dy):
        """Return a dx wide enough to fit all texts in the given dy."""
        zx, zy = self.zoom
        dy_char = zy * dy / len(texts)  # height of a char, in screen units
        dx_char = dy_char / 1.5  # approximate width of a char
        max_len = max(len(t) for t in texts)  # number of chars of the longest
        return max_len * dx_char / zx  # in tree units

    # These are the 2 functions that the user overloads to choose what to draw
    # when representing a node and a group of collapsed nodes:

    def draw_node(self, node, point, bdy):  # bdy: branch dy (height)
        """Yield graphic elements to draw the contents of the node."""
        yield from []  # only drawn if the node's content is visible

    def draw_collapsed(self):
        """Yield graphic elements to draw the nodes in self.collapsed."""
        yield from []  # they are always drawn (only visible nodes can collapse)
        # Uses self.collapsed and self.outline to extract and place info.


def read_label(label):
    expression, node_type, position, column = label

    assert node_type in ['leaf', 'internal', 'any'], \
        f'invalid node type: {node_type}'
    assert position in ['top', 'bottom', 'left', 'right', 'aligned'], \
        f'invalid position: {position}'

    return Label(
        code=compile(expression, '<string>', 'eval'),
        text_type='label_'+expression,  # will be used to set its looks in css
        node_type=node_type,  # type of nodes to apply this label to
        position=position,
        column=int(column))



class DrawerRect(Drawer):
    """Minimal functional drawer for a rectangular representation."""

    TYPE = 'rect'

    def in_viewport(self, box):
        if not self.viewport:
            return True

        if self.panel == 0:
            return intersects_box(self.viewport, box)
        else:
            return intersects_segment(get_ys(self.viewport), get_ys(box))

    def node_size(self, node):
        """Return the size of a node (its content and its children)."""
        return Size(node.size[0], node.size[1])

    def content_size(self, node):
        """Return the size of the node's content."""
        return Size(dist(node), node.size[1])

    def is_small(self, box):
        zx, zy = self.zoom
        return box.dy * zy < self.MIN_SIZE

    def get_box(self, element):
        return get_rect(element)

    def draw_distline(self, p1, p2, parent_of):
        """Yield a line representing a length."""
        line = draw_line(p1, p2, 'distline', parent_of)
        if not self.viewport or intersects_box(self.viewport, get_rect(line)):
            yield line

    def draw_childrenline(self, p1, p2):
        """Yield a line spanning children that starts at p1 and ends at p2."""
        line = draw_line(p1, p2, 'childrenline')
        if not self.viewport or intersects_box(self.viewport, get_rect(line)):
            yield line

    def draw_nodedot(self, center):
        dot = draw_circle(center, radius=1, circle_type='nodedot')
        if not self.viewport or intersects_box(self.viewport, get_rect(dot)):
            yield dot

    def draw_nodebox(self, node, node_id, box, result_of):
        yield draw_nodebox(box, node.name, node.props, node_id, result_of)



class DrawerCirc(Drawer):
    """Minimal functional drawer for a circular representation."""

    TYPE = 'circ'

    def __init__(self, tree, viewport=None, panel=0, zoom=(1, 1),
                 limits=None, collapsed_ids=None, labels=None, searches=None):
        super().__init__(tree, viewport, panel, zoom,
                         limits, collapsed_ids, labels, searches)

        assert self.zoom[0] == self.zoom[1], 'zoom must be equal in x and y'

        if not limits:
            self.ymin, self.ymax = -pi, pi

        self.dy2da = (self.ymax - self.ymin) / self.tree.size[1]

    def in_viewport(self, box):
        if not self.viewport:
            return intersects_segment((-pi, +pi), get_ys(box))

        if self.panel == 0:
            return (intersects_box(self.viewport, circumrect(box)) and
                    intersects_segment((-pi, +pi), get_ys(box)))
        else:
            return intersects_angles(self.viewport, box)

    def draw_outline(self):
        """Yield outline with all the skeleton points."""
        yield from super().draw_outline(self.dy2da)

    def flush_outline(self, minimum_dr=0):
        """Return box outlining the collapsed nodes."""
        r, a, dr, da = super().flush_outline(minimum_dr)
        a1, a2 = clip_angles(a, a + da)
        return Box(r, a1, dr, a2 - a1)

    def node_size(self, node):
        """Return the size of a node (its content and its children)."""
        return Size(node.size[0], node.size[1] * self.dy2da)

    def content_size(self, node):
        """Return the size of the node's content."""
        return Size(dist(node), node.size[1] * self.dy2da)

    def is_small(self, box):
        z = self.zoom[0]  # zx == zy in this drawer
        r, a, dr, da = box
        return (r + dr) * da * z < self.MIN_SIZE

    def get_box(self, element):
        return get_asec(element)

    def draw_distline(self, p1, p2, parent_of):
        """Yield a line representing a length."""
        if -pi <= p1[1] < pi:  # NOTE: the angles p1[1] and p2[1] are equal
            yield draw_line(cartesian(p1), cartesian(p2), 'distline', parent_of)

    def draw_childrenline(self, p1, p2):
        """Yield an arc spanning children that starts at p1 and ends at p2."""
        (r1, a1), (r2, a2) = p1, p2
        a1, a2 = clip_angles(a1, a2)
        if a1 < a2:
            is_large = a2 - a1 > pi
            yield draw_arc(cartesian((r1, a1)), cartesian((r2, a2)),
                                     is_large, 'childrenline')

    def draw_nodedot(self, center):
        r, a = center
        if -pi < a < pi:
            yield draw_circle(cartesian(center), radius=1,
                              circle_type='nodedot')

    def draw_nodebox(self, node, node_id, box, result_of):
        r, a, dr, da = box
        a1, a2 = clip_angles(a, a + da)
        if a1 < a2:
            yield draw_nodebox(Box(r, a1, dr, a2 - a1),
                               node.name, node.props, node_id, result_of)


def clip_angles(a1, a2):
    """Return the angles such that a1 to a2 extend at maximum from -pi to pi."""
    EPSILON = 1e-8  # without it, p1 can be == p2 and svg arcs are not drawn
    return max(-pi + EPSILON, a1), min(pi - EPSILON, a2)


def cartesian(point):
    r, a = point
    return r * cos(a), r * sin(a)


def polar(point):
    x, y = point
    return sqrt(x*x + y*y), atan2(y, x)


def is_good_angle_interval(a1, a2):
    EPSILON = 1e-8  # without it, rounding can fake a2 > pi
    return -pi <= a1 < a2 < pi + EPSILON


# Drawing generators.

def draw_rect_leaf_name(drawer, node, point):
    """Yield name to the right of the leaf."""
    if not node.is_leaf or not node.name:
        return

    x0, y0 = point
    dx0, dy0 = drawer.content_size(node)

    x = (x0 + dx0) if drawer.panel == 0 else drawer.xmin
    dx = drawer.dx_fitting_texts([node.name], dy0)
    y, dy = y0, dy0
    box = Box(x, y, dx, dy)

    zx, zy = drawer.zoom
    if dx * zx > drawer.MIN_SIZE_CONTENT and dy * zy > drawer.MIN_SIZE_CONTENT:
        yield draw_text(box, (0, 0.5), node.name, 'name')


def draw_circ_leaf_name(drawer, node, point):
    """Yield name at the end of the leaf."""
    if not node.is_leaf or not node.name:
        return

    r0, a0 = point
    dr0, da0 = drawer.content_size(node)

    if is_good_angle_interval(a0, a0 + da0) and r0 + dr0 > 0:
        r = (r0 + dr0) if drawer.panel == 0 else drawer.xmin
        dr = drawer.dx_fitting_texts([node.name], (r0 + dr0) * da0)
        a, da = a0, da0
        box = Box(r, a, dr, da)
        z = drawer.zoom[0]  # zx == zy
        if r > 0 and (dr * z > drawer.MIN_SIZE_CONTENT and
                      (r + dr) * da * z > drawer.MIN_SIZE_CONTENT):
            yield draw_text(box, (0, 0.5), node.name, 'name')


def draw_rect_collapsed_names(drawer):
    """Yield names of collapsed nodes after their outline."""
    x0, y0, dx0, dy0 = drawer.outline

    names = summary(drawer.collapsed)
    if all(name == '' for name in names):
        return

    all_have = all(node.name for node in drawer.collapsed)
    texts = text_repr(names, all_have)

    x = (x0 + dx0) if drawer.panel == 0 else drawer.xmin
    dx = drawer.dx_fitting_texts(texts, dy0)
    y, dy = y0, dy0
    box = Box(x, y, dx, dy)

    zx, zy = drawer.zoom
    if dx * zx > drawer.MIN_SIZE_CONTENT and dy * zy > drawer.MIN_SIZE_CONTENT:
        yield from draw_texts(box, (0, 0.5), texts, 'name')


def draw_circ_collapsed_names(drawer):
    """Yield names of collapsed nodes after their outline."""
    r0, a0, dr0, da0 = drawer.outline
    if not (-pi <= a0 <= pi and -pi <= a0 + da0 <= pi):
        return

    names = summary(drawer.collapsed)
    if all(name == '' for name in names):
        return

    all_have = all(node.name for node in drawer.collapsed)
    texts = text_repr(names, all_have)

    r = (r0 + dr0) if drawer.panel == 0 else drawer.xmin
    dr = drawer.dx_fitting_texts(texts, (r0 + dr0) * da0)
    a, da = a0, da0
    box = Box(r, a, dr, da)

    z = drawer.zoom[0]  # zx == zy
    if r > 0 and (dr * z > drawer.MIN_SIZE_CONTENT and
                  (r + dr) * da * z > drawer.MIN_SIZE_CONTENT):
        yield from draw_texts(box, (0, 0.5), texts, 'name')


def text_repr(names, all_have):
    if all_have:
        return names if len(names) < 6 else (names[:3] + ['...'] + names[-2:])
    else:
        return names[:6] + ['[...]']


def iter_labels(labels):
    """Yield the labels and their grid coordinates."""
    positions = set(x.position for x in labels)
    for pos in positions:
        labels_at_pos = [x for x in labels if x.position == pos]

        columns = sorted(set(x.column for x in labels_at_pos))
        ncols = len(columns)  # number of columns
        for icol, col in enumerate(columns):
            labels_at_col = [x for x in labels_at_pos if x.column == col]

            nrows = len(labels_at_col)  # number of rows in this column
            for irow, label in enumerate(labels_at_col):
                yield label, pos, icol, ncols, irow, nrows


# The actual drawers.

class DrawerRectLabels(DrawerRect):
    def draw_node(self, node, point, bdy):
        content_box = make_box(point, self.content_size(node))
        zx, zy = self.zoom

        labels = [label for label in self.labels
                      if is_valid_label(label, node.is_leaf, self.panel)]

        # Get labels, taking into account where they go.
        for label, pos, icol, ncols, irow, nrows in iter_labels(labels):
            text = get_label_text(label.code, node)
            box, anchor = get_label_placement(content_box, bdy, pos,
                                              icol, ncols, irow, nrows)

            if box.dx <= 0:  # fix dx of box
                x, y, dx, dy = box
                dx = self.dx_fitting_texts([text], dy)
                box = x + 10/zx, y, dx, dy

            _, _, dx, dy = box
            if text and (dx * zx > self.MIN_SIZE_CONTENT and
                         dy * zy > self.MIN_SIZE_CONTENT):
                yield draw_text(box, anchor, text, label.text_type)

    def draw_collapsed(self):
        x0, y0, dx0, dy0 = self.outline
        zx, zy = self.zoom

        labels = [label for label in self.labels
                      if is_valid_label(label, True, self.panel)]

        # Get labels, taking into account how many there are in each position.
        for pos in set(label.position for label in labels):
            labels_at_pos = [label for label in labels if label.position == pos]

            n = len(labels_at_pos)
            assert n <= 1, 'cannot show collapsed labels for more than 1 label'

            label = labels_at_pos[0]

            label_texts = summary(self.collapsed, label.code)
            if all(label_texts == '' for text in label_texts):
                return

            texts = text_repr(label_texts, all_have=True)  # TODO: fix all_have

            x = (x0 + dx0) if self.panel == 0 else self.xmin
            dx = self.dx_fitting_texts(texts, dy0)
            y, dy = y0, dy0
            box = Box(x, y, dx, dy)

            if texts and (dx * zx > self.MIN_SIZE_CONTENT and
                          dy * zy > self.MIN_SIZE_CONTENT):
                yield from draw_texts(box, (0, 0.5), texts, label.text_type)


class DrawerCircLabels(DrawerCirc):
    def draw_node(self, node, point, bda):
        content_box = make_box(point, self.content_size(node))
        z = self.zoom[0]  # zx == zy

        r0, a0 = point
        def fit(text, fs):
            return self.dx_fitting_texts([text], r0 * fs)

        labels = [label for label in self.labels
                      if is_valid_label(label, node.is_leaf, self.panel)]

        # Get labels, taking into account where they go.
        for label, pos, icol, ncols, irow, nrows in iter_labels(labels):
            text = get_label_text(label.code, node)
            box, anchor = get_label_placement(content_box, bda, pos,
                                              icol, ncols, irow, nrows)

            if box.dx <= 0:  # fix dr of box
                r, a, dr, da = box
                box = r + 10/z, a, fit(text, da), da

            r, a, dr, da = box
            if text and (r > 0 and dr * z > self.MIN_SIZE_CONTENT and
                         (r + dr) * da * z > self.MIN_SIZE_CONTENT):
                yield draw_text(box, anchor, text, label.text_type)

    def draw_collapsed(self):
        r0, a0, dr0, da0 = self.outline

        labels = [label for label in self.labels
                      if is_valid_label(label, True, self.panel)]

        # Get labels, taking into account how many there are in each position.
        for pos in set(label.position for label in labels) & {'right', 'aligned'}:
            labels_at_pos = [label for label in labels if label.position == pos]

            n = len(labels_at_pos)
            assert n <= 1, 'cannot show collapsed labels for more than 1 label'

            label = labels_at_pos[0]

            label_texts = summary(self.collapsed, label.code)
            if all(label_texts == '' for text in label_texts):
                return

            texts = text_repr(label_texts, all_have=True)  # TODO: fix all_have

            r = (r0 + dr0) if self.panel == 0 else self.xmin
            dr = self.dx_fitting_texts(texts, (r0 + dr0) * da0)
            a, da = a0, da0
            box = Box(r, a, dr, da)

            if texts and r > 0 and (dr * z > self.MIN_SIZE_CONTENT and
                                    (r + dr) * da * z > self.MIN_SIZE_CONTENT):
                yield from draw_texts(box, (0, 0.5), texts, label.text_type)


def is_valid_label(label, is_leaf, panel):
    pos, ntype = label.position, label.node_type

    is_valid_panel = ((pos != 'aligned' and panel == 0) or
                      (pos == 'aligned' and panel == 1))
    is_valid_ntype = ((ntype == 'any') or
                      (ntype == 'leaf' and is_leaf) or
                      (ntype == 'internal' and not is_leaf))

    return is_valid_panel and is_valid_ntype


def get_label_text(code, node):
    return str(safer_eval(code, {
        'name': node.name, 'is_leaf': node.is_leaf,
        'length': node.dist, 'dist': node.dist, 'd': node.dist,
        'support': node.props.get('support', ''),
        'properties': node.props, 'props': node.props, 'p': node.props,
        'get': dict.get, 'split': str.split,
        'children': node.children, 'ch': node.children,
        'regex': re.search,
        'len': len, 'sum': sum, 'abs': abs, 'float': float, 'pi': pi}))

def safer_eval(code, context):
    """Return a safer version of eval(code, context)."""
    for name in code.co_names:
        if name not in context:
            raise SyntaxError('invalid use of %r during evaluation' % name)
    return eval(code, {'__builtins__': {}}, context)


def get_label_placement(box, bdy, position, icol, ncols, irow, nrows):
    """Return a box and anchor to place a label."""
    # That corresponds to a node whose content is covered by box, has a
    # branch dy bdy, we want to place at the given position, in the
    # icol column of ncols, and the irow row of that column with nrows.

    x, y, dx, dy = box

    if position == 'top':
        return (Box(x + icol * dx / ncols, y + irow * bdy / nrows,
                    dx / ncols, bdy / nrows),  # above the branch
                (0.5, 1))  # halfway x, bottom y
    elif position == 'bottom':
        return (Box(x + icol * dx / ncols, y + bdy + irow * (dy - bdy) / nrows,
                    dx / ncols, (dy - bdy) / nrows),  # below the branch
                (0.5, 0))  # halfway x, top y
    elif position == 'left':
        return (Box(x - (icol + 1) * dx / ncols, y + irow * dy / nrows,
                    dx / ncols, dy / nrows),  # left of the branch
                (1, bdy / dy))  # right x, aligned y to bdy
    elif position == 'right':  # dx set to special value 0 -> ignored
        return (Box(x + dx, y + irow * dy / nrows,
                    0, dy / nrows),  # right of node
                (0, bdy / dy))  # left x, branch positionition in y
    elif position == 'aligned':  # dx set to special value 0 -> ignored
        return (Box(0, y + i * dy / nrows, 0, dy / nrows),
                (0, 0.5))
    else:
        raise ValueError(f'unkown position: {position}')


class DrawerRectLeafNames(DrawerRectLabels):
    def draw_node(self, node, point, bdy):
        yield from super().draw_node(node, point, bdy)

        if self.panel == 0:
            yield from draw_rect_leaf_name(self, node, point)

    def draw_collapsed(self):
        yield from draw_rect_collapsed_names(self)


class DrawerCircLeafNames(DrawerCircLabels):
    def draw_node(self, node, point, bda):
        yield from super().draw_node(node, point, bda)
        yield from draw_circ_leaf_name(self, node, point)

    def draw_collapsed(self):
        yield from draw_circ_collapsed_names(self)


class DrawerAlignNames(DrawerRectLabels):
    NPANELS = 2

    def draw_node(self, node, point, bdy):
        if self.panel == 0:
            yield from super().draw_node(node, point, bdy)

            if node.is_leaf and self.viewport:
                x, y = point
                dx, dy = self.content_size(node)
                p1 = (x + dx, y + dy/2)
                p2 = (self.viewport.x + self.viewport.dx, y + dy/2)
                yield draw_line(p1, p2, 'dotted')
        elif self.panel == 1:
            yield from draw_rect_leaf_name(self, node, point)

    def draw_collapsed(self):
        names = summary(self.collapsed)
        if all(name == '' for name in names):
            return

        if self.panel == 0:
            if self.viewport:
                x, y, dx, dy = self.outline
                p1 = (x + dx, y + dy/2)
                p2 = (self.viewport.x + self.viewport.dx, y + dy/2)
                yield draw_line(p1, p2, 'dotted')
        elif self.panel == 1:
            yield from draw_rect_collapsed_names(self)


class DrawerCircAlignNames(DrawerCircLabels):
    NPANELS = 2

    def draw_node(self, node, point, bda):
        if self.panel == 0:
            yield from super().draw_node(node, point, bda)
        elif self.panel == 1:
            yield from draw_circ_leaf_name(self, node, point)

    def draw_collapsed(self):
        if self.panel == 1:
            yield from draw_circ_collapsed_names(self)


# NOTE: The next three drawers (DrawerAlignFitness and
#   DrawerAlignFitnessMean, DrawerAlignFitnessCircular) are only there
#   as an example for a possible representation of "fitness".

from statistics import median, mean

class DrawerAlignFitness(DrawerRectLeafNames):
    NPANELS = 2

    def draw_node(self, node, point, bdy):
        if self.panel == 0:
            yield from super().draw_node(node, point, bdy)
        elif self.panel == 1 and node.is_leaf:
            _, y = point
            zx, zy = self.zoom
            if 'fitness' in node.props:  # and 1 * zy > self.MIN_SIZE_CONTENT:
                fitness = float(node.props['fitness'])
                yield from draw_fitness(fitness, self.xmin, y, dy=1)

    def draw_collapsed(self):
        if self.panel == 0:
            yield from super().draw_collapsed()
        elif self.panel == 1:
            _, y, _, _, dy = self.outline
            zx, zy = self.zoom
            values = [float(n.props['fitness'])
                          for n in self.collapsed if 'fitness' in n.props]
            if values:  # and dy * zy > self.MIN_SIZE_CONTENT:
                fitness = median(values)
                yield from draw_fitness(fitness, self.xmin, y, dy)

class DrawerAlignFitnessMean(DrawerAlignFitness):
    def draw_collapsed(self):
        if self.panel == 0:
            yield from super().draw_collapsed()
        elif self.panel == 1:
            _, y, _, _, dy = self.outline
            zx, zy = self.zoom
            values = [float(n.props['fitness'])
                          for n in self.collapsed if 'fitness' in n.props]
            if values:  # and dy * zy > self.MIN_SIZE_CONTENT:
                fitness = mean(values)
                yield from draw_fitness(fitness, self.xmin, y, dy)

class DrawerCircAlignFitness(DrawerCircLeafNames):
    NPANELS = 2

    def draw_node(self, node, point, bdy):
        if self.panel == 0:
            yield from super().draw_node(node, point, bdy)
        elif self.panel == 1 and node.is_leaf:
            r, a = point
            dr, da = self.content_size(node)
            z = self.zoom[0]  # zx == zy
            if 'fitness' in node.props and r > 0:
                # and dr * z > self.MIN_SIZE_CONTENT
                # and (r + dr) * da * z > self.MIN_SIZE_CONTENT:
                fitness = float(node.props['fitness'])
                yield from draw_fitness(fitness, self.xmin, a, dy=1)

    def draw_collapsed(self):
        if self.panel == 0:
            yield from super().draw_collapsed()
        elif self.panel == 1:
            r, a, dr, da = self.outline
            z = self.zoom[0]  # zx == zy
            values = [float(n.props['fitness'])
                          for n in self.collapsed if 'fitness' in n.props]
            if values and r > 0:
                # and dr * z > self.MIN_SIZE_CONTENT
                # and (r + dr) * da * z > self.MIN_SIZE_CONTENT:
                fitness = median(values)
                yield from draw_fitness(fitness, self.xmin, a, da)

def draw_fitness(fitness, xmin, y, dy):
    box = Box(xmin, y, 40, dy)
    hue = int((fitness + 10) * 10) % 360 if fitness <= 0 else 190  # arbitrary!
    yield draw_array(box, [hue])
    yield draw_text(box, (0, 0.5), '%.2f' % fitness, 'panel1')


# NOTE: The next two drawers (DrawerAlignHeatMap and DrawerCircAlignHeatMap)
#   are only there as an example for how to represent gene array data and so on
#   with heatmaps, but not really useful now (as opposed to the previous ones!).

class DrawerAlignHeatMap(DrawerRectLabels):
    NPANELS = 2

    def draw_node(self, node, point, bdy):
        if self.panel == 0:
            yield from super().draw_node(node, point, bdy)

            yield from draw_rect_leaf_name(self, node, point)
        elif self.panel == 1 and node.is_leaf:
            x, y = point
            dx, dy = self.content_size(node)
            random.seed(node.name)
            array = [random.randint(1, 360) for i in range(300)]
            yield draw_array(Box(0, y, 600, dy), array)

    def draw_collapsed(self):
        if self.panel == 0:
            yield from draw_rect_collapsed_names(self)
        elif self.panel == 1:
            x, y, dx, dy = self.outline
            text = ''.join(first_value(node) for node in self.collapsed)
            random.seed(text)
            array = [random.randint(1, 360) for i in range(300)]
            yield draw_array(Box(0, y, 600, dy), array)


class DrawerCircAlignHeatMap(DrawerCircLabels):
    NPANELS = 2

    def draw_node(self, node, point, bda):
        if self.panel == 0:
            yield from super().draw_node(node, point, bda)

            yield from draw_circ_leaf_name(self, node, point)
        elif self.panel == 1 and node.is_leaf:
            r, a = point
            dr, da = self.content_size(node)
            random.seed(node.name)
            array = [random.randint(1, 360) for i in range(50)]
            box = Box(1.5 * self.xmin, a, 20 * self.tree.size[0], da)
            yield draw_array(box, array)

    def draw_collapsed(self):
        if self.panel == 0:
            yield from draw_circ_collapsed_names(self)
        elif self.panel == 1:
            r, a, dr, da = self.outline
            text = ''.join(first_value(node) for node in self.collapsed)
            random.seed(text)
            array = [random.randint(1, 360) for i in range(50)]
            box = Box(1.5 * self.xmin, a, 20 * self.tree.size[0], da)
            yield draw_array(box, array)


def get_drawers():
    return [
        DrawerRect, DrawerCirc,
        DrawerRectLabels, DrawerCircLabels,
        DrawerRectLeafNames, DrawerCircLeafNames,
        DrawerAlignNames, DrawerCircAlignNames,
        DrawerAlignHeatMap, DrawerCircAlignHeatMap,
        DrawerAlignFitness, DrawerAlignFitnessMean,
        DrawerCircAlignFitness]


def summary(nodes, code=None):
    """Return a list of values summarizing the given list of nodes."""
    values_dict = OrderedDict((first_value(n, code), None) for n in nodes)
    return list(values_dict.keys())


def first_value(tree, code=None):
    """Return value of evaluating the given code, on the first possible node."""
    if code is None:  # special (and common!) case: get the first name
        return next((node.name for node in tree.traverse() if node.name), '')
    else:
        for node in tree.traverse():
            value = get_label_text(code, node)
            if value:
                return value
        return ''


def draw_texts(box, anchor, texts, text_type):
    """Yield texts so they fit in the box."""
    dy = box.dy / len(texts)
    y = box.y
    for text in texts:
        yield draw_text(Box(box.x, y, box.dx, dy), anchor, text, text_type)
        y += dy


# Basic drawing elements.

def draw_nodebox(box, name='', props=None, node_id=None, result_of=None):
    return ['nodebox', box, name, props or {}, node_id or [], result_of or []]

def draw_outline(points):
    return ['outline', points]

def draw_line(p1, p2, line_type='', parent_of=None):
    return ['line', p1, p2, line_type, parent_of or []]

def draw_arc(p1, p2, large=False, arc_type=''):
    return ['arc', p1, p2, int(large), arc_type]

def draw_circle(center, radius, circle_type=''):
    return ['circle', center, radius, circle_type]

def draw_text(box, anchor, text, text_type=''):
    return ['text', box, anchor, text, text_type]

def draw_array(box, a):
    return ['array', box, a]


# Box-related functions.

def make_box(point, size):
    x, y = point
    dx, dy = size
    return Box(x, y, dx, dy)

def get_xs(box):
    x, _, dx, _ = box
    return x, x + dx

def get_ys(box):
    _, y, _, dy = box
    return y, y + dy


def get_rect(element):
    """Return the rectangle that contains the given graphic element."""
    eid = element[0]
    if eid in ['nodebox', 'array', 'text']:
        return element[1]
    elif eid == 'outline':
        points = element[1]
        x, y = points[0]
        return Box(x, y, 0, 0)  # we don't care for the rect of an outline
    elif eid in ['line', 'arc']:  # not a great approximation for an arc...
        (x1, y1), (x2, y2) = element[1], element[2]
        return Box(min(x1, x2), min(y1, y2), abs(x2 - x1), abs(y2 - y1))
    elif eid == 'circle':
        (x, y), r = element[1], element[2]
        return Box(x, y, 0, 0)
    else:
        raise ValueError(f'unrecognized element: {element!r}')


def get_asec(element):
    """Return the annular sector that contains the given graphic element."""
    eid = element[0]
    if eid in ['nodebox', 'array', 'text']:
        return element[1]
    elif eid == 'outline':
        points = element[1]
        r, a = points[0]
        return Box(r, a, 0, 0)  # we don't care for the rect of an outline
    elif eid in ['line', 'arc']:
        (x1, y1), (x2, y2) = element[1], element[2]
        rect = Box(min(x1, x2), min(y1, y2), abs(x2 - x1), abs(y2 - y1))
        return circumasec(rect)
    elif eid == 'circle':
        (x, y), r = element[1], element[2]
        rect = Box(x, y, 0, 0)
        return circumasec(rect)
    else:
        raise ValueError(f'unrecognized element: {element!r}')


def drawn_size(elements, get_box):
    """Return the size of a box containing all the elements."""
    # The type of size will depend on the kind of boxes that are returned by
    # get_box() for the elements. It is width and height for boxes that are
    # rectangles, and dr and da for boxes that are annular sectors.
    if not elements:
        return Size(0, 0)

    x, y, dx, dy = get_box(elements[0])
    x_min, x_max = x, x + dx
    y_min, y_max = y, y + dy

    for element in elements[1:]:
        x, y, dx, dy = get_box(element)
        x_min, x_max = min(x_min, x), max(x_max, x + dx)
        y_min, y_max = min(y_min, y), max(y_max, y + dy)

    return Size(x_max - x_min, y_max - y_min)


def intersects_box(b1, b2):
    """Return True if the boxes b1 and b2 (of the same kind) intersect."""
    return (intersects_segment(get_xs(b1), get_xs(b2)) and
            intersects_segment(get_ys(b1), get_ys(b2)))


def intersects_segment(s1, s2):
    """Return True if the segments s1 and s2 intersect."""
    s1min, s1max = s1
    s2min, s2max = s2
    return s1min <= s2max and s2min <= s1max


def intersects_angles(rect, asec):
    """Return True if any part of rect is contained within the asec angles."""
    return any(intersects_segment(get_ys(circumasec(r)), get_ys(asec))
                   for r in split_thru_negative_xaxis(rect))
    # We divide rect in two if it passes thru the -x axis, because then its
    # circumbscribing asec goes from -pi to +pi and (wrongly) always intersects.


def split_thru_negative_xaxis(rect):
    """Return a list of rectangles resulting from cutting the given one."""
    x, y, dx, dy = rect
    if x >= 0 or y > 0 or y + dy < 0:
        return [rect]
    else:
        EPSILON = 1e-8
        return [Box(x, y, dx, -y-EPSILON), Box(x, EPSILON, dx, dy + y)]


def stack(box1, box2):
    """Return the box resulting from stacking the given boxes."""
    if not box1:
        return box2
    else:
        x, y, dx1, dy1 = box1
        _, _, dx2, dy2 = box2
        return Box(x, y, max(dx1, dx2), dy1 + dy2)


def points_from_nodes(nodes, point, dy_min, dy2da=1, maxdepth=30):
    """Return the points outlining the given nodes, starting at point."""
    x, y = point  # top-left origin at point
    dx, dy = 0, 0  # defined here so they can be accessed inside add_points()
    points = []  # the actual points we are interested in
    abox = [x, y, 0, 0]  # box surrounding the accumulated node boxes
    def add_points(ps):
        if abox[-1] > dy_min:
            points.extend(corner_points(*abox))
        points.extend(ps)
        abox[:] = [x, y+dy, 0, 0]

    for node in nodes:
        dx, dy = node.size[0], node.size[1] * dy2da
        if maxdepth < 0:  # we went too far: add bounding box
            add_points(corner_points(x, y, dx, dy))
        elif dy < dy_min:  # too small to peek inside
            if dy < 0.2 * dy_min:  # and even to try to draw
                ax, ay, adx, ady = abox
                abox = [ax, ay, max(adx, dx), ady+dy]  # accumulate it!
            else:  # small, but no too small
                add_points(corner_points(x, y, dx, dy))  # just add bounding box
        else:
            add_points(points_from_node(node, (x, y), dy_min,
                                        dy2da, maxdepth-1))
        y += dy

    if abox[-1] > 0:  # do we still have any accumulated box of points?
        points.extend(corner_points(*abox))  # flush remaining points

    if len(nodes) < 2:
        return points
    else:
        by = (points[0][1] + points[-1][1]) / 2
        return [(x, by)] + points + [(x, by)]


def points_from_node(node, point, dy_min, dy2da=1, maxdepth=30):
    """Return the points outlining the given node, starting at point."""
    x, y = point
    dx, dy = dist(node), node.size[1] * dy2da

    points = points_from_nodes(node.children, (x + dx, y), dy_min,
                               dy2da, maxdepth-1)

    by = ((points[0][1] + points[-1][1]) / 2) if points else (y + dy/2)

    return ([(x, by), (x + dx, by)] +
            points +
            [(x + dx, by), (x, by)])


def corner_points(x, y, dx, dy):
    """Return the corner points of a box at (x, y) and dimensions (dx, dy)."""
    return [(x, y),                   # 1    1,5.-----.2
                       (x+dx, y),     # 2       |     |
                       (x+dx, y+dy),  # 3       |     |
            (x, y+dy),                # 4      4·-----·3
            (x, y)]                   # 5 (same as 1, closing the box)


def dist(node):
    """Return the distance of a node, with default values if not set."""
    return float(node.props.get('dist', 0 if node.up is None else 1))


def circumrect(asec):
    """Return the rectangle that circumscribes the given annular sector."""
    if asec is None:
        return None

    rmin, amin, dr, da = asec
    rmax, amax = rmin + dr, amin + da

    amin, amax = clip_angles(amin, amax)

    points = [(rmin, amin), (rmin, amax), (rmax, amin), (rmax, amax)]

    xs = [r * cos(a) for r,a in points]
    ys = [r * sin(a) for r,a in points]

    xmin, ymin = min(xs), min(ys)
    xmax, ymax = max(xs), max(ys)

    if amin < -pi/2 < amax:  # asec traverses the -y axis
        ymin = -rmax
    if amin < 0 < amax:  # asec traverses the +x axis
        xmax = rmax
    if amin < pi/2 < amax:  # asec traverses the +y axis
        ymax = rmax
    # NOTE: the annular sectors we consider never traverse the -x axis.

    return Box(xmin, ymin, xmax - xmin, ymax - ymin)


def circumasec(rect):
    """Return the annular sector that circumscribes the given rectangle."""
    if rect is None:
        return None

    x, y, dx, dy = rect

    points = [(x, y), (x, y+dy), (x+dx, y), (x+dx, y+dy)]
    radius2 = [x*x + y*y for x,y in points]

    if x <= 0 and x+dx >= 0 and y <= 0 and y+dy >= 0:
        return Box(0, -pi, sqrt(max(radius2)), 2*pi)
    else:
        angles = [atan2(y, x) for x,y in points]
        rmin, amin = sqrt(min(radius2)), min(angles)

        return Box(rmin, amin, sqrt(max(radius2)) - rmin, max(angles) - amin)
