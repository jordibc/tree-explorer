"""
Tree-related operations.

Sorting, changing the root to a node, moving branches, removing (prunning)...
"""

from collections import namedtuple, deque

import ete.tree as ete_tree


def copy(tree):
    """Return a copy of the tree."""
    return ete_tree.Tree(tree.props,
                         children=[copy(node) for node in tree.children])


def sort(tree, key=None, reverse=False):
    """Sort the tree in-place."""
    key = key or (lambda node: (node.size[1], node.size[0], node.name))

    for node in traverse(tree, order=+1):
        node.children.sort(key=key, reverse=reverse)


def root_at(node, bprops=None):
    """Set the given node as the root of the tree.

    The original root node will be used as the new root node, so any
    reference to it in the code will still be valid.

    bprops is a list of branch properties (other than "dist" and "support").
    """
    root = node.root

    if root is node:
        return  # nothing to do!

    assert_root_consistency(root, bprops)

    positions = node.id  # child positions from root to node (like [1, 0, ...])

    interchange_references(root, node)  # root <--> node
    old_root = node  # now "node" points to where the old root was

    current_root = old_root  # current root, which will change in each iteration
    for child_pos in positions:
        current_root = rehang(current_root, child_pos, bprops)

    if len(old_root.children) == 1:
        join_branch(old_root)


def interchange_references(node1, node2):
    """Interchange the references of the given nodes.

    node1 will point where node2 was, and viceversa.
    """
    if node1 is node2:
        return

    # Interchange properties.
    node1.props, node2.props = node2.props, node1.props

    # Interchange children.
    node1.children, node2.children = node2.children, node1.children

    # Interchange parents.
    up1 = node1.up
    up2 = node2.up
    pos1 = up1.children.index(node1) if up1 else None
    pos2 = up2.children.index(node2) if up2 else None

    if up1 is not None:
        up1.children.pop(pos1)
        up1.children.insert(pos1, node2)

    if up2 is not None:
        up2.children.pop(pos2)
        up2.children.insert(pos2, node1)

    node1.up = up2
    node2.up = up1


def set_outgroup(node, bprops=None, dist=None):
    """Change tree so the given node is set as outgroup.

    The original root node will be used as the new root node, so any
    reference to it in the code will still be valid.

    node is the node to set as outgroup (future first child of the root).
    bprops is a list of branch properties (other than "dist" and "support").
    dist is the distance from the node, where we put the new root of the tree.
    """
    assert node.up is not None, 'cannot set the absolute tree root as outgroup'
    assert_root_consistency(node.root, bprops)

    intermediate = ete_tree.Tree()
    insert_intermediate(node, intermediate, bprops, dist)

    root_at(intermediate, bprops)


def assert_root_consistency(root, bprops=None):
    """Raise AssertionError if the root node of a tree looks inconsistent."""
    assert root.dist in [0, None], 'root has a distance'

    for pname in ['support'] + (bprops or []):
        assert pname not in root.props, f'root has branch property: {pname}'

    if len(root.children) == 2:
        s1, s2 = [n.support for n in root.children]
        assert s1 == s2, 'inconsistent support at the root: %r != %r' % (s1, s2)


def rehang(root, child_pos, bprops=None):
    """Rehang root on its child at position child_pos and return it."""
    # root === child  ->  child === root
    child = root.children.pop(child_pos)
    child.children.append(root)

    swap_props(root, child, ['dist', 'support'] + (bprops or []))

    return child  # which is now the new root


def swap_props(n1, n2, props):
    """Swap properties between nodes n1 and n2."""
    for pname in props:
        p1 = n1.props.pop(pname, None)
        p2 = n2.props.pop(pname, None)
        if p1 is not None:
            n2.props[pname] = p1
        if p2 is not None:
            n1.props[pname] = p2


def insert_intermediate(node, intermediate, bprops=None, dist=None):
    """Insert, between node and its parent, an intermediate node."""
    # == up ======= node  ->  == up === intermediate === node
    up = node.up

    pos_in_parent = up.children.index(node)  # save its position in parent
    up.children.pop(pos_in_parent)  # detach from parent

    intermediate.children.append(node)

    if 'dist' in node.props:  # split dist between the new and old nodes
        if dist is not None:
            node.dist, intermediate.dist = dist, node.dist - dist
        else:
            node.dist = intermediate.dist = node.dist / 2

    for prop in ['support'] + (bprops or []):  # copy other branch props if any
        if prop in node.props:
            intermediate.props[prop] = node.props[prop]

    up.children.insert(pos_in_parent, intermediate)  # put new where the old was


def join_branch(node):
    """Substitute node for its only child."""
    # == node ==== child  ->  ====== child
    assert len(node.children) == 1, 'cannot join branch with multiple children'

    child = node.children[0]

    if 'support' in node.props or 'support' in child.props:
        s1, s2 = node.support, child.support
        assert s1 == s2, f'cannot join branches, different support: {s1}, {s2}'

    if 'dist' in node.props:
        child.dist = (child.dist or 0) + node.dist  # restore total dist

    up = node.up
    pos_in_parent = up.children.index(node)  # save its position in parent
    up.children.pop(pos_in_parent)  # detach from parent
    up.children.insert(pos_in_parent, child)  # put child where the old node was


def move(node, shift=1):
    """Change the position of the current node with respect to its parent."""
    # ╴up╶┬╴node     ->  ╴up╶┬╴sibling
    #     ╰╴sibling          ╰╴node
    assert node.up, 'cannot move the root'

    siblings = node.up.children

    pos_old = siblings.index(node)
    pos_new = (pos_old + shift) % len(siblings)

    siblings[pos_old], siblings[pos_new] = siblings[pos_new], siblings[pos_old]


def remove(node):
    """Remove the given node from its tree."""
    assert node.up, 'cannot remove the root'

    up = node.up
    up.children.remove(node)


def to_dendrogram(tree):
    """Convert tree to dendrogram (remove all distance values)."""
    for node in traverse(tree):
        node.props.pop('dist', None)


def to_ultrametric(tree, topological=False):
    """Convert tree to ultrametric (all leaves equally distant from root)."""
    tree.dist = tree.dist or 0  # covers common case of root not having dist set

    update_sizes_all(tree)  # so node.size[0] are distances to leaves

    dist_full = tree.size[0]  # original distance from root to furthest leaf

    if (topological or dist_full <= 0 or
        any(node.dist is None for node in traverse(tree))):
        # Ignore original distances and just use the tree topology.
        for node in traverse(tree):
            node.dist = 1 if node.up else 0
        update_sizes_all(tree)
        dist_full = dist_full if dist_full > 0 else tree.size[0]

    for node in traverse(tree):
        if node.dist > 0:
            d = sum(n.dist for n in ancestors(node, tree))
            node.dist *= (dist_full - d) / node.size[0]


def resolve_polytomy(tree, descendants=True):
    """Convert tree to a series of dicotomies if it is a polytomy.

    A polytomy is a node that has more than 2 children. This
    function changes them to a ladderized series of dicotomic
    branches. The tree topology modification is arbitrary (no
    important results should depend on it!).

    descendants, if True, resolves all polytomies in the tree,
    including all root descendants. Otherwise, only for the root.
    """
    for node in traverse(tree):
        if len(node.children) > 2:
            children = node.children  #  x ::: a,b,c,d,e
            node.children = []

            # Create "backbone" nodes:  x --- * --- * ---
            for i in range(len(children) - 2):
                node_new = ete_tree.Tree({'dist': 0, 'support': 0})
                node.children.append(node_new)
                node = node_new

            # Add children in order:  x === d,* === c,* === b,a
            node.children.append(children[0])  # first:  x --- * --- * --- a
            for i in range(1, len(children)):
                children[i].support = 0
                node.children.append(children[i])
                node = node.up

        if not descendants:
            break


def robinson_foulds(t1, t2, prop='name', strict=False):
    """Return the Robinson-Foulds comparison between trees t1 and t2.

    The comparison is a named tuple with the distance, the maximum
    possible distance (useful for normalizing), the common values used
    for comparing (usually names of common leaves), and the partitions
    that exist in each tree (the sorted tuples of leaves seen at both
    sides of each branch).

    The distance is the number of partitions not shared by both trees.

    prop is the property used to identify the leaves.
    strict, if True, requires that t1 and t2 share all leaves.
    """
    common_vals = get_common_values(t1, t2, prop, strict)

    parts1 = make_partitions(t1, prop, common_vals)
    parts2 = make_partitions(t2, prop, common_vals)

    dist = len(parts1 ^ parts2)  # size of the symmetric difference

    dist_max = 3 * len(common_vals) - 6  # maximum possible distance

    RFResult = namedtuple('RFResult', 'dist max common parts1 parts2')

    return RFResult(dist, dist_max, common_vals, parts1, parts2)


def get_common_values(t1, t2, prop='name', strict=False):
    """Return the common leaf property prop values of trees t1 and t2.

    If strict, raise AssertionError if t1 and t2 don't share leaves.
    """
    vals1 = set(n.props.get(prop) for n in t1.leaves())  # can be names
    vals2 = set(n.props.get(prop) for n in t2.leaves())
    common_vals = vals1 & vals2  # common leaf values of property prop

    assert not strict or (
        len(common_vals) == len(vals1) == len(vals2) == len(t1) == len(t2)), \
        (f'all leaves should have a different {prop}, the same in both trees '
         '(use strict=False otherwise)')

    assert None not in common_vals, f'all leaves should have property {prop}'

    return common_vals


def make_partitions(tree, prop='name', valid_values=None):
    """Return a set of partitions of the given tree.

    A "partition" is an id for each branch, based on the leaves that it
    has at each side. The id is unique no matter the topology.

    :param prop: Property used to identify the leaves.
    :param valid_values: Set of valid leaf values to use in the
        partitions. If None, use all leaf values from tree.
    """
    valid_values = (valid_values if valid_values is not None else
                    set(n.props.get(prop) for n in tree.leaves()))
    partitions = set()
    values = {}  # dict of leaf values for property prop under each node
    for node in traverse(tree, order=+1):
        if node.is_leaf:
            v = node.props.get(prop)
            leaf_values = {v} if v in valid_values else set()
        else:
            leaf_values = set.union(*(values[n] for n in node.children))
            for n in node.children:
                values.pop(n)  # free memory, no need to keep all the values

        values[node] = leaf_values  # saved for future use by its parent node

        partitions.add(partition_id(leaf_values, valid_values - leaf_values))

    return partitions


def partition_id(values1, values2):
    """Return a unique id based on the given sets of values."""
    side1 = tuple(sorted(values1))  # id for one side
    side2 = tuple(sorted(values2))  # id for the other side
    return tuple(sorted([side1, side2]))  # joint id: the two ids sorted


def farthest_descendant(tree, topological=False):
    """Return the farthest descendant and its distance."""
    d = (lambda node: 1) if topological else (lambda node: node.dist)  # dist

    dist_root = {tree: 0}  # will contain all distances to the root

    node_farthest, dist_farthest = tree, 0
    for node in traverse(tree, order=-1):  # traverse in preorder
        if node is not tree:
            dist_root[node] = dist = dist_root[node.up] + d(node)
            if dist > dist_farthest:
                node_farthest, dist_farthest = node, dist

    return node_farthest, dist_farthest


def farthest(tree, topological=False):
    """Return the farthest nodes and the diameter of the tree."""
    d = (lambda node: 1) if topological else (lambda node: node.dist)  # dist

    def last(x):
        return x[-1]  # return the last element (used later for comparison)

    # Part 1: Find the farthest descendant for all nodes.

    fd = {}  # dict of {node: (farthest_descendant, dist_from_parent_to_it)}
    for node in traverse(tree, order=+1):  # traverse in postorder
        if node.is_leaf:
            fd[node] = (node, d(node))
        else:
            f_leaf, dist = max((fd[n] for n in node.children), key=last)
            fd[node] = (f_leaf, (d(node) if node is not tree else 0) + dist)

    # Part 2: Find the extremes and the diameter.

    # The first extreme is fixed. The second and the diameter will be updated.
    extreme1, diameter = fd[tree]  # first extreme: farthest node from the root
    extreme2 = tree  # so far, but may change later

    # Go towards the root, updating the second extreme and diameter.
    prev = extreme1  # the node that we saw previous to the current one
    curr = extreme1.up  # the current node we are visiting
    d_curr_e1 = d(extreme1)  # distance from current to the 1st extreme
    while curr is not tree.up:
        leaf, dist = max((fd[n] for n in curr.children if n is not prev),
                         default=(curr, 0), key=last)
        if dist + d_curr_e1 > diameter:
            extreme2, diameter = leaf, dist + d_curr_e1

        d_curr_e1 += d(curr) if curr is not tree else 0
        prev, curr = curr, curr.up

    return extreme1, extreme2, diameter


def midpoint(tree, topological=False):
    """Return the node in the middle and its distance from the exact center."""
    d = (lambda node: 1) if topological else (lambda node: node.dist)

    # Find the farthest node and diameter.
    node, _, diameter = farthest(tree, topological)

    # Go thru ancestor nodes until we cover more distance than the tree radius.
    dist = diameter / 2 - d(node)  # radius of the tree minus branch dist
    while dist > 0:
        node = node.up
        dist -= d(node)

    return node, dist + d(node)  # NOTE: `dist` is negative


def set_midpoint_outgroup(tree, topological=False):
    node, dist = midpoint(tree, topological)
    set_outgroup(node, dist=dist)


def ancestors(node, root=None, include_root=True):
    """Yield all ancestor nodes of the given node (up to root if given)."""
    # root === n1 === n2 === ... === nN === node  ->  [nN, ..., n2, n1, root]
    if node is root:
        return  # node is not an ancestor of itself

    while node.up is not root:
        node = node.up
        yield node

    if root is not None and include_root:
        yield root


def lineage(node, root=None, include_root=True):
    """Yield all nodes in the lineage of node (up to root if given)."""
    if not include_root and node is root:
        return  # the node itself would not be in its lineage

    # Same as ancestors() but also yielding itself first.
    yield node
    yield from ancestors(node, root, include_root)


def common_ancestor(nodes):
    """Return the last node common to the lineages of the given nodes."""
    if not nodes:
        return None

    curr = nodes[0]  # current node being the last common ancestor

    for node in nodes[1:]:
        lin_node = set(lineage(node))
        curr = next((n for n in lineage(curr) if n in lin_node), None)

    return curr  # which is now the last common ancestor of all nodes


def dist(node1, node2, topological=False):
     """Return the distance between node1 and node2."""
     # Function to get the distance of a node.
     d = (lambda node: 1) if topological else (lambda node: node.dist)

     root = common_ancestor([node1, node2])  # common root
     assert root is not None, 'nodes do not belong to the same tree'

     return (sum(d(n) for n in lineage(node1, root, include_root=False)) +
             sum(d(n) for n in lineage(node2, root, include_root=False)))


# Traversing the tree.

def traverse(tree, order=-1, is_leaf_fn=None):
    """Yield nodes with a depth-first search, pre (< 0) or post (> 0) order."""
    visiting = [(tree, False)]
    while visiting:
        node, seen = visiting.pop()

        is_leaf = is_leaf_fn(node) if is_leaf_fn else node.is_leaf

        if is_leaf or (order <= 0 and not seen) or (order >= 0 and seen):
            yield node

        if not seen and not is_leaf:
            visiting.append((node, True))  # add node back, but mark as seen
            visiting += [(n, False) for n in node.children[::-1]]


def traverse_bfs(tree, is_leaf_fn=None):
    """Yield nodes with a breadth-first search (level order traversal)."""
    visiting = deque([tree])
    while visiting:
        node = visiting.popleft()
        yield node
        if not is_leaf_fn or not is_leaf_fn(node):
            visiting.extend(node.children)


# Position on the tree: current node, number of visited children.
TreePos = namedtuple('TreePos', 'node nch')

class Walker:
    """Represents the position when traversing a tree."""

    def __init__(self, root):
        self.visiting = [TreePos(node=root, nch=0)]
        # will look like: [(root, 2), (child2, 5), (child25, 3), (child253, 0)]
        self.descend = True

    def go_back(self):
        self.visiting.pop()
        if self.visiting:
            node, nch = self.visiting[-1]
            self.visiting[-1] = TreePos(node, nch + 1)
        self.descend = True

    @property
    def node(self):
        return self.visiting[-1].node

    @property
    def node_id(self):
        return tuple(branch.nch for branch in self.visiting[:-1])

    @property
    def first_visit(self):
        return self.visiting[-1].nch == 0

    @property
    def has_unvisited_branches(self):
        node, nch = self.visiting[-1]
        return nch < len(node.children)

    def add_next_branch(self):
        node, nch = self.visiting[-1]
        self.visiting.append(TreePos(node=node.children[nch], nch=0))


def walk(tree):
    """Yield an iterator as it traverses the tree."""
    it = Walker(tree)  # node iterator
    while it.visiting:
        if it.first_visit:
            yield it

            if it.node.is_leaf or not it.descend:
                it.go_back()
                continue

        if it.has_unvisited_branches:
            it.add_next_branch()
        else:
            yield it
            it.go_back()


# Size-related functions.

def update_sizes_from(node):
    """Update sizes from the given node to the root of the tree."""
    while node is not None:
        update_size(node)
        node = node.up


def update_sizes_all(tree):
    """Update sizes of all the nodes in the tree."""
    for node in traverse(tree, order=+1):
        update_size(node)


def update_size(node):
    """Update the size of the given node."""
    sumdists, nleaves = get_size(node.children)
    dx = float(node.props.get('dist', 0 if node.up is None else 1)) + sumdists
    node.size = (dx, max(1, nleaves))


def get_size(nodes):
    """Return the size of all the nodes stacked."""
    # The size of a node is (sumdists, nleaves) with sumdists the dist to
    # its furthest leaf (including itself) and nleaves its number of leaves.

    # If we were to use cython, we would change "def get_size..." to
    # "cdef (double, double) get_size", and also add the following line:
    #   cdef double sumdists, nleaves

    sumdists = 0
    nleaves = 0
    for node in nodes:
        sumdists = max(sumdists, node.size[0])
        nleaves += node.size[1]

    return sumdists, nleaves
