"""
Class to represent trees (which are nodes connected to other nodes) and
basic functions related to them.
"""

from . import newick, text_viz, explorer, operations as ops


cdef class Tree:
    cdef public Tree up  # parent
    cdef public dict props
    cdef public Children _children
    cdef public (double, double) size  # sum of lenghts, number of leaves

    def __init__(self, data=None, children=None, parser=None, **kwargs):
        self.up = None
        self.children = children or []

        data = data.read().strip() if hasattr(data, 'read') else data

        valid_ks = ['name', 'dist', 'support'] if data is None else []
        assert all(k in valid_ks for k in kwargs), \
            f'invalid keyword in: {list(kwargs.keys())}'

        assert data is None or type(data) in [dict, str], \
            f'can only read data from dict or string, but got {type(data)}'

        if data is None:
            self.props = kwargs.copy()
        elif type(data) == dict:
            self.props = data.copy()
        else:
            assert not children, 'init from newick cannot have children'
            tree = newick.loads(data, parser)
            self.children = tree.children
            self.props = tree.props

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, children):
        self._children = Children(self, children)

    @property
    def nrepr(self):
        return newick.content_repr(self)

    @nrepr.setter
    def nrepr(self, content):
        self.props = newick.get_props(content, self.is_leaf)

    @property
    def name(self):
        return str(self.props.get('name')) if 'name' in self.props else None

    @name.setter
    def name(self, value):
        if value is not None:
            self.props['name'] = str(value)
        else:
            self.props.pop('name', None)

    @property
    def dist(self):
        return float(self.props['dist']) if 'dist' in self.props else None

    @dist.setter
    def dist(self, value):
        if value is not None:
            self.props['dist'] = float(value)
        else:
            self.props.pop('dist', None)

    @property
    def support(self):
        return float(self.props['support']) if 'support' in self.props else None

    @support.setter
    def support(self, value):
        if value is not None:
            self.props['support'] = float(value)
        else:
            self.props.pop('support', None)

    @property
    def is_leaf(self):
        """Return True if the current node is a leaf."""
        return not self.children

    @property
    def is_root(self):
        """Return True if the current node has no parent."""
        return self.up is None

    @property
    def root(self):
        """Return the absolute root node of the current tree structure."""
        node = self
        while node.up is not None:
            node = node.up
        return node

    @property
    def id(self):
        """Return this node id (list of branch positions from the root)."""
        reversed_id = []
        node = self
        while node.up is not None:
            reversed_id.append(node.up.children.index(node))
            node = node.up
        return reversed_id[::-1]

    @property
    def level(self):
        """Return the number of nodes between this node and the root."""
        n = 0
        node = self.up
        while node is not None:
            n += 1
            node = node.up
        return n

    def walk(self):
        return ops.walk(self)

    def explore(self, name=None, quiet=True):
        explorer.explore(self, name, quiet)

    def copy(self):
        return ops.copy(self)

    def traverse(self, strategy='preorder', is_leaf_fn=None):
        """Traverse the tree structure under this node and yield the nodes.

        There are three possible strategies. There is a breadth-first
        search (BFS) named "levelorder", and two depth-first searches
        (DFS) named "preorder" and "postorder".
        """
        if strategy == 'preorder':
            yield from ops.traverse(self, order=-1, is_leaf_fn=is_leaf_fn)
        elif strategy == 'postorder':
            yield from ops.traverse(self, order=+1, is_leaf_fn=is_leaf_fn)
        elif strategy == 'levelorder':
            yield from ops.traverse_bfs(self, is_leaf_fn)
        else:
            raise ValueError(f'unknown strategy: {strategy}')

    def leaves(self):
        """Yield the terminal nodes (leaves) under this node."""
        for node in ops.traverse(self):
            if node.is_leaf:
                yield node

    def __bool__(self):
        # If this is not defined, bool(t) will call len(t) (terribly slow!).
        return True

    def __len__(self):
        """Return the number of leaves."""
        return sum(1 for _ in self.leaves())

    def __iter__(self):
        """Yield all the terminal nodes (leaves)."""
        yield from self.leaves()

    def __getitem__(self, node_id):
        """Return the node that matches the given node_id, or None."""
        if type(node_id) == str:    # node_id can be the name of a node
            return next((n for n in self.traverse() if n.name == node_id), None)
        elif type(node_id) == int:  # or the index of a child
            return self.children[node_id]
        else:                       # or a list/tuple of the (sub-sub-...)child
            node = self
            for i in node_id:
                node = node.children[i]
            return node

    def __repr__(self):
        name_str = (' ' + repr(self.name)) if self.name else ''
        return '<Tree%s at %s>' % (name_str, hex(self.__hash__()))

    def __str__(self):
        return self.to_str()

    def to_str(self, *args, **kwargs):
        return text_viz.to_str(self, *args, **kwargs)


cdef class Children(list):
    """A list that sets the parent of its elements."""

    cdef public Tree up  # parent

    def __init__(self, up, nodes=()):
        super().__init__(nodes)
        self.up = up  # remember the parent for future addition of nodes
        self.update()

    def update(self):
        """Assign self.up as parent of all elements."""
        up, children = self.up, self  # clearer notation

        for node in children:
            node.up = up

    def append(self, node):
        super().append(node)
        self.update()

    def insert(self, index, node):
        super().insert(index, node)
        self.update()

    def extend(self, nodes):
        super().extend(nodes)
        self.update()

    def pop(self, index=-1):
        node = super().pop(index)
        node.up = None
        self.update()
        return node

    def remove(self, node):
        node.up = None
        super().remove(node)
        self.update()

    def __iadd__(self, nodes):
        super().__iadd__(nodes)
        self.update()
        return self
