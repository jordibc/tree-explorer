"""
Parser for trees represented in newick format.
"""

# See https://en.wikipedia.org/wiki/Newick_format

from .tree import Tree


class NewickError(Exception):
    pass


# Interpret and represent the content of a node in newick format.

def quote(name, escaped_chars=" \t\r\n()[]':;,"):
    """Return the name quoted if it has any characters that need escaping."""
    if any(c in name for c in escaped_chars):
        return "'%s'" % name.replace("'", "''")  # ' escapes to '' in newicks
    else:
        return name

def unquote(name):
    """Return the name unquoted if it was quoted."""
    name = str(name).strip()

    if name.startswith("'") and name.endswith("'"):  # standard quoting with '
        return name[1:-1].replace("''", "'")  # ' escapes to '' in newicks
    elif name.startswith('"') and name.endswith('"'):  # non-standard quoting "
        return name[1:-1].replace('""', '"')
    else:
        return name


NAME    = {'name': 'name',    'read': unquote, 'write': quote}
DIST    = {'name': 'dist',    'read': float, 'write': lambda x: '%g' % float(x)}
SUPPORT = {'name': 'support', 'read': float, 'write': lambda x: '%g' % float(x)}

PARSER_DEFAULT = {  # what 'p0:p1' means (props and how to read/write them)
    'leaf':     [NAME, DIST],  # ((name:dist)x:y);
    'internal': [NAME, DIST],  # ((x:y)name:dist);  but could be  support:dist
}


def content_repr(node, props=None, parser=None):
    """Return content of a node as represented in newick format."""
    parser = parser or PARSER_DEFAULT
    prop0, prop1 = parser['leaf' if node.is_leaf else 'internal']

    p0_name, p0_write = prop0['name'], prop0['write']  # shortcuts
    p1_name, p1_write = prop1['name'], prop1['write']

    p0_str = p0_write(node.props[p0_name]) if p0_name in node.props else ''
    p1_str = p1_write(node.props[p1_name]) if p1_name in node.props else ''

    props = props if props is not None else node.props  # overwrite
    pairs_str = ':'.join('%s=%s' % (k, node.props[k]) for k in props
                         if k in node.props and k not in [p0_name, p1_name])

    return (p0_str + (f':{p1_str}' if p1_str else '') +     # p0:p1
            (f'[&&NHX:{pairs_str}]' if pairs_str else ''))  # [&&NHX:p2=x:p3=y]


def get_props(content, is_leaf, parser=None):
    """Return the properties from the content (as a newick) of a node.

    Example (for the default format of a leaf node):
      'abc:123[&&NHX:x=foo]'  ->  {'name': 'abc', 'dist': 123, 'x': 'foo'}
    """
    parser = parser or PARSER_DEFAULT
    prop0, prop1 = parser['leaf' if is_leaf else 'internal']

    p0_name, p0_read = prop0['name'], prop0['read']  # shortcuts
    p1_name, p1_read = prop1['name'], prop1['read']

    props = {}  # will contain the properties extracted from the content string

    p0_str, pos = read_content(content, 0, endings=':[')

    if p0_str:
        props[p0_name] = p0_read(p0_str)

    if pos < len(content) and content[pos] == ':':
        pos = skip_spaces_and_comments(content, pos+1)
        p1_str, pos = read_content(content, pos, endings='[ ')
        props[p1_name] = p1_read(p1_str)

    pos = skip_spaces_and_comments(content, pos)

    if pos < len(content) and content[pos] == '[':
        pos_end = content.find(']', pos+1)
        props.update(get_extended_props(content[pos+1:pos_end]))
    elif pos < len(content):
        raise NewickError('malformed content: %s' % repr_short(content))

    return props


def get_extended_props(text):
    """Return a dict with the properties extracted from the text in NHX format.

    Example: '&&NHX:x=foo:y=bar'  ->  {'x': 'foo', 'y': 'bar'}
    """
    try:
        assert text.startswith('&&NHX:'), 'unknown annotation -- not "&&NHX"'
        return dict(pair.split('=') for pair in text[len('&&NHX:'):].split(':'))
    except (AssertionError, ValueError) as e:
        raise NewickError('invalid NHX format (%s) in text %s' %
                          (e, repr_short(text)))


def repr_short(obj, max_len=50):
    """Return a representation of the given object, limited in length."""
    # To use for messages in exceptions.
    txt = repr(obj)
    return txt if len(txt) < max_len else txt[:max_len-10] + ' ... ' + txt[-5:]


# Parsing.

def load(fp, parser=None):
    return loads(fp.read().strip(), parser)


def loads(tree_text, parser=None):
    """Return tree from its newick representation."""
    if not tree_text.endswith(';'):
        raise NewickError('text ends with no ";"')

    if tree_text[0] == '(':
        nodes, pos = read_nodes(tree_text, parser, 0)
    else:
        nodes, pos = [], 0

    content, pos = read_content(tree_text, pos)
    if pos != len(tree_text) - 1:
        raise NewickError(f'root node ends at position {pos}, before tree ends')

    return Tree(get_props(content, not nodes, parser), nodes)


def read_nodes(nodes_text, parser, int pos=0):
    """Return a list of nodes and the position in the text where they end."""
    # nodes_text looks like '(a,b,c)', where any element can be a list of nodes
    if nodes_text[pos] != '(':
        raise NewickError('nodes text starts with no "("')

    nodes = []
    while nodes_text[pos] != ')':
        pos += 1
        if pos >= len(nodes_text):
            raise NewickError('nodes text ends missing a matching ")"')

        pos = skip_spaces_and_comments(nodes_text, pos)

        if nodes_text[pos] == '(':  # this element is a list of nodes
            children, pos = read_nodes(nodes_text, parser, pos)
        else:  # this element is a leaf
            children = []

        content, pos = read_content(nodes_text, pos)

        nodes.append(Tree(get_props(content, not children, parser), children))

    return nodes, pos+1


def skip_spaces_and_comments(text, int pos):
    """Return position in text after pos and all whitespaces and comments."""
    # text = '...  [this is a comment] node1...'
    #            ^-- pos               ^-- pos (returned)
    while pos < len(text) and text[pos] in ' \t\r\n[':
        if text[pos] == '[':
            if text[pos+1] == '&':  # special annotation
                return pos
            else:
                pos = text.find(']', pos+1)  # skip comment
        pos += 1  # skip whitespace and comment endings

    return pos


def read_content(str text, int pos, endings=',);'):
    """Return content starting at position pos in text, and where it ends."""
    # text = '...(node_1:0.5[&&NHX:p=a],...'  ->  'node_1:0.5[&&NHX:p=a]'
    #             ^-- pos              ^-- pos (returned)
    start = pos

    if pos < len(text) and text[pos] in ["'", '"']:
        pos = skip_quoted_name(text, pos)

    while pos < len(text) and text[pos] not in endings:
        pos += 1

    return text[start:pos], pos


def skip_quoted_name(str text, int pos):
    """Return the position where a quoted name ends."""
    # text = "... 'node ''2'' in tree' ..."
    #             ^-- pos             ^-- pos (returned)
    if pos >= len(text) or text[pos] not in ["'", '"']:
        raise NewickError(f'text at position {pos} does not start with quote')

    start = pos
    q = text[start]  # quoting character (can be ' or ")

    while pos+1 < len(text):
        pos += 1

        if text[pos] == q:
            # Newick format escapes ' as '' (and we generalize to q -> qq)
            if pos+1 >= len(text) or text[pos+1] != q:
                return pos+1  # that was the closing quote
            else:
                pos += 1  # that was an escaped quote - skip

    raise NewickError('unfinished quoted name: %s' % repr_short(text[start:]))


def dumps(tree, props=None, parser=None):
    """Return newick representation of the given tree."""
    children_str = ','.join(dumps(node, props, parser).rstrip(';')
                            for node in tree.children)

    return ((f'({children_str})' if tree.children else '') +
            content_repr(tree, props, parser) + ';')


def dump(tree, fp, parser=None):
    fp.write(dumps(tree, parser))
