Tree Explorer is a tool for phylogenetic trees exploration.

In addition to the tree manipulation and drawing modules (`ete.tree`
and `ete.draw`), it includes a web server that manages trees, exposes
an api and serves an interactive gui accessible from your web
browser. It is described further below.


# Installing

## Fast/modern way

You can install directly from the gitlab repository with:

```sh
pip3 install https://gitlab.com/jordibc/tree-explorer/-/archive/ete-like/tree-explorer-ete-like.zip
```

Like most python packages, instead of installing it globally it is
possible to install it locally in a virtual environment running first
`python3 -m venv env`, and `source env/bin/activate` (or,
alternatively, with [virtualenv](https://virtualenv.pypa.io/)).

Finally, you can also download the package (with `git clone` or
otherwise) and in the package's directory run the following command to
install all dependencies and compile the module locally:

```sh
pip3 install -e .
```


## Step by step (without pip)

### Prerequisites

The tree and drawing modules just need [Python
3](https://www.python.org/downloads/) and
[Cython](https://cython.org/).

To use the web server, you need in addition the following python
modules: bottle, brotli.

In a debian-based system you can add all of them with `apt install
cython3 python3-bottle python3-brotli`.


### Module

To create the module `tree` and the parser `newick` you need to first
compile them with Cython:

```sh
python3 setup.py build_ext --inplace
```

After that, you will only need to run that command if you ever change
the source files (`ete/tree.pyx` or `ete/newick.pyx`).


# Quick tree exploration

You can run the explorer directly with:

```sh
./ete/explorer.py <file>
```

which will load the tree(s) from the file (a newick, a nexus, or a
compressed file containing any combination of them), start the web
server and open a browser to start exploring it.


# Web server

The web server exposes an api to create, query, and change trees. It
also provides methods to draw and manipulate interactively the trees,
which are used in the interactive gui that it serves by default at
`http://localhost:5000`.


## Backend for multiple trees and users

You can also run the server to efficiently handle multiple connections
from multiple users. To do that, you can run it for example with
[gunicorn](https://gunicorn.org/) in python like this:

```py
from ete import tree, explorer
explorer.run(server='gunicorn', host='0.0.0.0', port=5000)
```

In this example, it will listen to exterior connections to port 5000
and handle them.


## Api

The api has (among others) the following endpoints:

```text
/trees
/trees/<id>
/trees/<id>/newick
/trees/<id>/size
/trees/<id>/draw
```

Some endpoints accept different [request
methods](https://en.wikipedia.org/wiki/HTTP#Request_methods). Most
endpoints accept the GET method to **query** information. The `/trees`
endpoint also accepts the POST method to **create** trees. Some
endpoints (like `/trees/<id>/sort`) accept the PUT method to
**modify** them.

All the requests must send the information as json (with the
`Content-Type: application/json` header). The responses are also json-encoded.

Most reponses include, in addition to the appropriate [HTTP status
code](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes), a key
named `message` in the response. If the request was successful, its
value will be `ok`. If not, it will include a description of the kind
of error.

### Example calls

You can use [httpie](https://httpie.io/) (which is like `curl` if you prefer,
with a modern syntax) to test the backend with commands like:

```sh
http :5000/trees name=test_tree newick='(a:1,b:2);'  # POST call: add tree

http :5000/trees/test_tree  # GET call: query information

http ':5000/trees/test_tree/draw?zy=10'  # get graphic elements

http PUT :5000/trees/test_tree/rename --raw '[[0], "x"]'  # PUT call: modify

http :5000/trees/test_tree/newick  # here we would see the modified tree
```


# Tests

You can run the set of tests in the `tests` directory with:

```sh
pytest-3
```

which will run all the functions that start with `test_` in the
`test_*.py` files. You can also use the contents of those files to
see examples of how to use ete.


# License

This program is licensed under the GPL v3. See the [project
license](docs/license.md) for further details.
